<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use PHPUnit\Util\RegularExpression;

class EmailSubDomain implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('^[a-zA-Z0-9_.+-]+(@viacesi.fr|@cesi.fr|@ismans.cesi.fr)+$^', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "L'adresse mail doit se terminer par @cesi.fr, @viacesi.fr ou @ismans.cesi.fr";
    }
}
