<?php

namespace App\Exports;

use App\Models\Utilisateur;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class UtilisateurExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Utilisateur::where('email_verified_at','>', 0)->get();
    }

    public function columnFormats(): array
    {
        return [
            'E' => DataType::TYPE_STRING,
        ];
    }
}
