<?php

namespace App\Exports;

use App\Models\Modele;
use App\Models\Marque;
use Maatwebsite\Excel\Concerns\FromCollection;

class ModeleExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $modeles = Modele::addSelect(['nom_marque' => Marque::select('nom_marque')->whereColumn('id_marque','modele.id_marque')->limit(1)])->get();
        return $modeles;
    }
}
