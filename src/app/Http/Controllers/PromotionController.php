<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use App\Models\Utilisateur;
use App\Models\Formation;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        $promotions = Promotion::all();
        foreach( $promotions as $promotion)
        {
            $promotion->utilisateur ?? "";
            $promotion->formation ?? "";
        }
        return json_encode($promotions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Promotion::create([
            'code_promotion' => $request['code_promotion'],
            'date_debut' => $request['date_debut'],
            'date_fin' => $request['date_fin'],
            'id_formation' => $request['id_formation'],
            'id_utilisateur_referent' => $request['id_utilisateur'],

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        return $promotion;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $promotion->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        $promotion->delete();
    }
}
