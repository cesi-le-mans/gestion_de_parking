<?php

namespace App\Http\Controllers;

use App\Models\Typologie;
use Illuminate\Http\Request;

class TypologieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return Typologie::all()->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $typologie = new Typologie($request->all());
        $typologie->save();

        return $typologie->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  Typologie  $typologie
     * @return \Illuminate\Http\Response
     */
    public function show(Typologie $typologie)
    {
        return $typologie;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Typologie  $typologie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Typologie $typologie)
    {
        $typologie->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Typologie  $typologie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Typologie $typologie)
    {
        $typologie->delete();
        if (strpos($request['url'], 'admin') != false) {
            return redirect($request['url']);
        }
    }

    }
