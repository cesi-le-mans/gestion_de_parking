<?php

namespace App\Http\Controllers;

use App\Models\Modele;
use App\Models\Vehicule;
use Illuminate\Http\Request;

class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        $vehicules = Vehicule::all();
        foreach ($vehicules as $vehicule){
            $vehicule->modele ?? '';
            $vehicule->modele->marque ?? '';
            $vehicule->type ?? '';
            $vehicule->typologie ?? '';
            $vehicule->utilisateur ?? '';
        }
        return $vehicules->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehicule = Vehicule::create([
            'immatriculation' => $request['immatriculation'],
            'couleur' => $request['couleur'],
            'id_utilisateur' => $request['id_utilisateur'],
            'id_type_vehicule' => $request['id_type_vehicule'],
            'id_modele' => $request['id_modele'],
            'id_typologie_vehicule' => $request['id_typologie_vehicule'],

        ]);

        return response()->json(array('id' => $vehicule->immatriculation));
    }

    /**
     * Display the specified resource.
     *
     * @param  Vehicule $vehicule
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicule $vehicule)
    {
        return $vehicule;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Vehicule $vehicule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicule $vehicule)
    {
        $vehicule->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicule $vehicule)
    {
        $vehicule->delete();
    }
}
