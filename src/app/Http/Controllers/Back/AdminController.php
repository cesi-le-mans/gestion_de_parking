<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\UtilisateursController;
use App\Models\Constantes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Use Models
use App\Models\Utilisateur;
use App\Models\Vehicule;
use App\Models\Type;
use App\Models\Typologie;
use App\Models\Signalement;
use App\Models\Role;
use App\Models\Promotion;
use App\Models\MotifSignalement;
use App\Models\MotifDemande;
use App\Models\Marque;
use App\Models\Modele;
use App\Models\Formation;
use App\Models\Demande;

//Excel
use App\Exports\UtilisateurExport;
use App\Imports\UtilisateurImport;
use App\Exports\ModeleExport;
use App\Imports\ModeleImport;
use App\Exports\MarqueExport;
use App\Imports\MarqueImport;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN');
    }
    public function index()
    {
        $vehicules = Vehicule::all()->take(8);
        foreach ($vehicules as $vehicule) {
            $vehicule->modele ?? '';
            $vehicule->modele->marque ?? '';
            $vehicule->type ?? '';
            $vehicule->typologie ?? '';
            $vehicule->utilisateur ?? '';
        }

        $utilisateurs = Utilisateur::all()->take(8);

        $demandes = Demande::all()->take(8);
        foreach ($demandes as $demande) {
            $demande->utilisateur->nom ?? '';
            $demande->utilisateur->prenom ?? '';
            $demande->motifDemande->libelle_motif_demande ?? '';
        }

        $signalements = Signalement::all()->where('signalement_traite','=',0)->take(8);
        foreach ($signalements as $signalement){
            $signalement->utilisateur ?? '';
            $signalement->motifSignalement ?? '';
            $signalement->vehicule ?? '';
            $signalement->vehicule->modele ?? '';
            $signalement->vehicule->modele->marque ?? '';
        }


        return view('back.index', ['vehicules' => $vehicules, 'utilisateurs' => $utilisateurs, 'demandes' => $demandes, 'signalements' => $signalements]);
    }

    public function users()
    {
        //On retourne seulement les utilisateurs qui on validé leur mail (obligé de passer par un array car la méthode whereNotNull retourne une collection)
        $users = Utilisateur::all();
        $usersWithVerifiedMail = array();


        foreach ($users as $user) {
            $user->promotion->code_promotion ?? '';
            $user->promotion->formation ?? '';
            $user->role->nom_role ?? '';
            if (!empty($user->email_verified_at)) {
                array_push($usersWithVerifiedMail, $user);
            }
        }
        $users = json_decode(json_encode($users));
        return view('back.utilisateur.user-management', ['utilisateurs' => $usersWithVerifiedMail, 'roles' => Role::all(), 'promotions' => Promotion::all()]);
    }
    public function formations()
    {
        //$formations = Formation::all();
        //return view('back.utilisateur.formation-management')->with('formations', $formations);
        return view('back.utilisateur.formation-management', ['formations' => Formation::all()]);
    }
    public function promotions()
    {
        $promotions = Promotion::all();
        foreach ($promotions as $promotion) {
            $promotion->formation ?? '';
            $promotion->utilisateur->nom ?? '';
        }
        $utilisateurs = Utilisateur::all()->whereNotNull('email_verified_at')->where('id_role', '=', Constantes::ID_ROLE_REFERENT);
        $formations = Formation::all();

        return view('back.utilisateur.promotion-management', ['promotions' => $promotions, 'utilisateurs' => $utilisateurs, 'formations' => $formations]);
    }
    public function roles()
    {
        return view('back.utilisateur.role-management', ['roles' => Role::all()]);
    }

    public function vehicules()
    {
        $vehicules = Vehicule::all();
        $modeles = Modele::all();
        foreach ($vehicules as $vehicule) {
            $vehicule->modele ?? '';
            $vehicule->modele->marque ?? '';
            $vehicule->type ?? '';
            $vehicule->typologie ?? '';
            $vehicule->utilisateur ?? '';
        }
        foreach ($modeles as $modele) {
            $modele->marque ?? '';
        }

        return view('back.vehicule.vehicule-management', ['vehicules' => $vehicules, 'types' => Type::all(), 'marques' => Marque::all(), 'modeles' => $modeles, 'typologies' => Typologie::all(), 'utilisateurs' => Utilisateur::all()]);
    }

    public function marques()
    {
        return view('back.vehicule.marque-management', ['marques' => Marque::all()]);
    }

    public function modeles()
    {
        $modeles = Modele::all();
        foreach ($modeles as $modele) {
            $modele->marque->nom_marque ?? '';
        }
        return view('back.vehicule.modele-management', ['modeles' => $modeles, 'marques' => Marque::all()]);
    }

    public function types()
    {
        return view('back.vehicule.type-management', ['types' => Type::all()]);
    }

    public function typologies()
    {
        return view('back.vehicule.typologie-management', ['typologies' => Typologie::all()]);
    }

    public function demandes()
    {
        $demandes = Demande::all();
        foreach ($demandes as $demande) {
            $demande->utilisateur ?? '';
            $demande->motifDemande ?? '';
        }
        return view('back.demande.demande-management', ['demandes' => $demandes]);
    }



    public function motifsDemandes()
    {
        return view('back.demande.motifDemande-management', ['motifsDemandes' => MotifDemande::all()]);
    }

    public function motifsSignalements()
    {
        $motifsSignalements = MotifSignalement::all();
        return view('back.signalement.motifSignalement-management', compact('motifsSignalements'));
    }




    public function signalements()
    {
        $signalements = Signalement::all()->take(8);
        foreach ($signalements as $signalement) {
            $signalement->utilisateur ?? '';
            $signalement->motifSignalement ?? '';
            $signalement->vehicule ?? '';
            $signalement->vehicule->utilisateur ?? '';
            $signalement->vehicule->modele ?? '';
            $signalement->vehicule->modele->marque ?? '';
        }
        return view('back.signalement.signalement-management', ['signalements' => $signalements]);
    }




    /**
     * @return \Illuminate\Support\Collection
     */
    public function utilisateurExport()
    {
        return Excel::download(new UtilisateurExport, 'utilisateur.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function utilisateurImport()
    {
        if(request()->file('file')){
            Excel::import(new UtilisateurImport, request()->file('file'));
            
        } 
        return back();
        
    }

        /**
     * @return \Illuminate\Support\Collection
     */
    public function modeleExport()
    {
        return Excel::download(new ModeleExport, 'modele.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function modeleImport()
    {
        if(request()->file('file')){
            Excel::import(new ModeleImport, request()->file('file'));
            
        } 
        return back();
        
    }

        /**
     * @return \Illuminate\Support\Collection
     */
    public function marqueExport()
    {
        return Excel::download(new MarqueExport, 'marque.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function marqueImport()
    {
        if(request()->file('file')){
            Excel::import(new MarqueImport, request()->file('file'));
            
        } 
        return back();
        
    }
}
