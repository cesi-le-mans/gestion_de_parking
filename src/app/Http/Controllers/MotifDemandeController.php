<?php

namespace App\Http\Controllers;

use App\Models\MotifDemande;
use Illuminate\Http\Request;

class MotifDemandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        $motifsDemande = MotifDemande::all();

        return $motifsDemande->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $motifDemande = new MotifDemande($request->all());
        $motifDemande->save();

        return $motifDemande->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  MotifDemande $motifDemande
     * @return \Illuminate\Http\Response
     */
    public function show(MotifDemande $motifDemande)
    {
        return $motifDemande;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  MotifDemande  $motifDemande
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MotifDemande $motifDemande)
    {
        $motifDemande->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  MotifDemande  $motifDemande
     * @return \Illuminate\Http\Response
     */
    public function destroy(MotifDemande $motifDemande)
    {
        $motifDemande->delete();
    }

}
