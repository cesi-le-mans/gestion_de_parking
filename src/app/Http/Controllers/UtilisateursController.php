<?php

namespace App\Http\Controllers;

use App\Models\Constantes;
use App\Models\Role;
use App\Models\Utilisateur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use mysql_xdevapi\Exception;
use function MongoDB\BSON\toJSON;

class UtilisateursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        //on retourne seulement les utilisateurs dont le mail est vérifié
        $users = Utilisateur::all()->whereNotNull('email_verified_at');
        foreach ($users as $user){
            $user->promotion->formation ?? '';
            $user->role->nom_role ?? '';
        }
        return $users->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Constantes::ID_ROLE_ETUDIANT;
        $request->validate([
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:utilisateur'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if(isset($request['id_role'])){
            $role = $request['id_role'];
        }
        $utilisateur = Utilisateur::create([
            'nom' => $request['nom'],
            'prenom' => $request['prenom'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'id_role' => $role,
            'utilisateur_handicape' => $request['utilisateur_handicape'] == "1" ? 1 : 0,
            'code_promotion' => $request['code_promotion'] ?? null,
            'telephone' => $request['telephone'] ?? '',
        ]);



        return response()->json(array('id' => $utilisateur->id_utilisateur));
    }

    /**
     * Display the specified resource.
     *
     * @param  Utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function show(Utilisateur $utilisateur)
    {
        $utilisateur->vehicules;
        foreach ($utilisateur->vehicules as $vehicule){
            $vehicule->modele ?? '';
            $vehicule->modele->marque ?? '';
            $vehicule->type ?? '';
            $vehicule->typologie ?? '';
        }
        $utilisateur->demandes;
        return $utilisateur;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Utilisateur $utilisateur)
    {
        $utilisateur->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Utilisateur $utilisateur)
    {
        $utilisateur->delete();
    }

}
