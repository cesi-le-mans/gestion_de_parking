<?php

namespace App\Http\Controllers;

use App\Models\MotifDemande;
use App\Models\MotifSignalement;
use Illuminate\Http\Request;

class MotifSignalementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return MotifSignalement::all()->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $motifSignalement = new MotifSignalement($request->all());
        $motifSignalement->save();

        return $motifSignalement->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  MotifSignalement  $motifSignalement
     * @return \Illuminate\Http\Response
     */
    public function show(MotifSignalement $motifSignalement)
    {
        return $motifSignalement;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  MotifSignalement  $motifSignalement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MotifSignalement $motifSignalement)
    {
        $motifSignalement->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  MotifSignalement  $motifSignalement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, MotifSignalement $motifSignalement)
    {
        $motifSignalement->delete();
        if (strpos($request['url'], 'admin') != false) {
            return redirect($request['url']);
        }
    }
}
