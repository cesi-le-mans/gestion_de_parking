<?php

namespace App\Http\Controllers;

use App\Models\Signalement;
use App\Models\Utilisateur;
use App\Models\Vehicule;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function MongoDB\BSON\toJSON;

class SignalementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return Signalement::all()->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $signalement = new Signalement();
        $signal = DB::table('signalement')->where('immatriculation', $request['immatriculation'])->where('date_signalement', date('Y-m-d'))->where('signalement_traite', 0)->first();
        if($signal == null) {


            if ($request->hasFile('signalement_image')) {


                $folder = '/uploads/images/';
                $imageName = Str::slug($request['nom_utilisateur']) . '_' . time() . '.' . $request->file('signalement_image')->getClientOriginalExtension();
                $request->file('signalement_image')->move(public_path($folder), $imageName);
                $signalement->signalement_image = $folder . $imageName;

            }
            $signalement->id_utilisateur = $request['id_utilisateur'];
            $signalement->immatriculation = $request['immatriculation'];
            $signalement->date_signalement = date('Y-m-d');
            $signalement->id_motif_signalement = $request['id_motif_signalement'];
            $signalement->signalement_traite = 0;
            $signalement->save();
        }
        else{
            return Response::HTTP_ALREADY_REPORTED;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Signalement  $signalement
     * @return \Illuminate\Http\Response
     */
    public function show(Signalement $signalement)
    {
        return $signalement;
    }

    /**
     * Traitement du signalement
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Signalement  $signalement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Signalement $signalement)
    {


        //on récupère toutes les infos interessantes :
        $referent = DB::table('utilisateur')->where('id_utilisateur',$signalement->vehicule->utilisateur->promotion->id_utilisateur_referent)->first();

        $utilisateur = $signalement->vehicule->utilisateur;
        $formation = $utilisateur->promotion->formation->nom_formation;
        $marque = $signalement->vehicule->modele->marque->nom_marque;
        $motif = $signalement->motifSignalement->libelle_motif_signalement;
        $modele = $signalement->vehicule->modele->nom_modele;
        $image = $signalement->signalement_image;


        $data = [
            'referent' => $referent,
            'apprennant' => $utilisateur,
            'motif' => $motif,
            'classe' => $formation,
            'immatriculation' => $signalement->immatriculation,
            'marque' => $marque,
            'modele' => $modele
        ];

        //envoi de mail à l'apprennant
        Mail::send('emails.signalementMailApprennant',['data' => $data], function($message) use ($image, $utilisateur) {
            $message->to($utilisateur->email)->subject
            ('Signalement de vehicule');
            if($image != null){
                $message->attach(public_path($image));
            }
        });

        //envoi mail referent
        Mail::send('emails.signalementMailReferent',['data' => $data], function($message) use ($image, $referent) {
            $message->to($referent->email)->subject
            ('Signalement de vehicule');
            if($image != null){
                $message->attach(public_path($image));
            }
        });
        $signalement->update(['signalement_traite' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Signalement  $signalement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Signalement $signalement)
    {
        $signalement->delete();
    }
}
