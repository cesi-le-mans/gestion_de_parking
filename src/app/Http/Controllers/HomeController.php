<?php

namespace App\Http\Controllers;

use App\Models\Demande;
use App\Models\Marque;
use App\Models\Modele;
use App\Models\MotifDemande;
use App\Models\MotifSignalement;
use App\Models\Type;
use App\Models\Typologie;
use App\Models\Utilisateur;
use App\Models\Vehicule;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $vehicules = Vehicule::all();
        $modeles = Modele::all();
        $demandes = Demande::all();
        foreach ($vehicules as $vehicule){
            $vehicule->modele ?? '';
            $vehicule->modele->marque ?? '';
            $vehicule->type ?? '';
            $vehicule->typologie ?? '';
            $vehicule->utilisateur ?? '';
        }
        foreach ($modeles as $modele){
            $modele->marque ?? '';
        }
        foreach ($demandes as $demande){
            $demande->motifDemande ?? '';
        }

        return view('home', ['vehicules' => $vehicules,
            'types' => Type::all(),
            'marques' => Marque::all(),
            'modeles' => $modeles,
            'typologies' => Typologie::all(),
            'utilisateurs' => Utilisateur::all(),
            'motifsDemande' => MotifDemande::all(),
            'motifsSignalement' => MotifSignalement::all(),
            'demandes' => $demandes]);
    }
}
