<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $role
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if(! $request->user()->hasRole($role)){
            abort(401, "Vous n'avez pas l'autorisation nécessaire");
        }
        return $next($request);
    }
}
