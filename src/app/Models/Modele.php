<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_modele
 * @property int $id_marque
 * @property string $nom_modele
 * @property Marque $marque
 */
class Modele extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modele';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_modele';

    /**
     * @var array
     */
    protected $fillable = ['id_marque', 'nom_modele'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function marque()
    {
        return $this->belongsTo('App\Models\Marque', 'id_marque', 'id_marque');
    }
}
