<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_formation
 * @property string $nom_formation
 * @property string $sigle_formation
 * @property Promotion[] $promotions
 */
class Formation extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'formation';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_formation';

    /**
     * @var array
     */
    protected $fillable = ['nom_formation', 'sigle_formation'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotions()
    {
        return $this->hasMany('App\Models\Promotion', 'id_formation', 'id_formation');
    }
}
