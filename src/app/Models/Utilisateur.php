<?php

namespace App\Models;

use DateTime;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id_utilisateur
 * @property int $id_role
 * @property string $code_promotion
 * @property string $email
 * @property string $nom
 * @property string $prenom
 * @property string $password
 * @property string $numero_telephone
 * @property boolean $utilisateur_handicape
 * @property string $token
 * @property DateTime $email_verified_at
 * @property Promotion $promotion
 * @property Role $role
 * @property Demande[] $demandes
 * @property Promotion[] $promotions
 * @property Vehicule[] $vehicules
 */
class Utilisateur extends Authenticatable implements MustVerifyEmail
{

    use HasFactory, Notifiable;
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'utilisateur';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_utilisateur';

    /**
     * @var array
     */
    protected $fillable = ['id_role', 'code_promotion', 'email', 'nom', 'prenom', 'password', 'numero_telephone', 'utilisateur_handicape', 'token', 'email_verified_at'];

    protected $hidden = [ 'password', 'token'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promotion()
    {
        return $this->belongsTo('App\Models\Promotion', 'code_promotion', 'code_promotion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'id_role', 'id_role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function demandes()
    {
        return $this->hasMany('App\Models\Demande', 'id_utilisateur', 'id_utilisateur');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotions()
    {
        return $this->hasMany('App\Models\Promotion', 'id_utilisateur_referent', 'id_utilisateur');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicules()
    {
        return $this->hasMany('App\Models\Vehicule', 'id_utilisateur', 'id_utilisateur');
    }

    public function setRememberToken($token)
    {
        // Set the remember token your own way...

        $this->token = $token;
    }

    public function getRememberToken()
    {
        return $this->token;
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, "Cette action n'est pas autorisée");
    }

    public function hasAnyRole($role)
    {
        if ($this->hasRole($role)) {
            return true;
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->role()->where('nom_role', $role)->first()) {
            return true;
        }
        return false;
    }
}
