<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_type_vehicule
 * @property string $type_vehicule
 * @property Vehicule[] $vehicules
 */
class Type extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_type_vehicule';

    /**
     * @var array
     */
    protected $fillable = ['type_vehicule'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicules()
    {
        return $this->hasMany('App\Models\Vehicule', 'id_type_vehicule', 'id_type_vehicule');
    }
}
