<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_role
 * @property string $nom_role
 * @property Utilisateur[] $utilisateurs
 */
class Role extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_role';

    /**
     * @var array
     */
    protected $fillable = ['nom_role'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function utilisateurs()
    {
        return $this->hasMany('App\Models\Utilisateur', 'id_role', 'id_role');
    }
}
