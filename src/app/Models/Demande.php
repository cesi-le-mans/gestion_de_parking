<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_demande
 * @property int $id_utilisateur
 * @property int $id_motif_demande
 * @property string $commentaire_demande
 * @property MotifDemande $motifDemande
 * @property Utilisateur $utilisateur
 */
class Demande extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demande';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_demande';

    /**
     * @var array
     */
    protected $fillable = ['id_utilisateur', 'id_motif_demande', 'commentaire_demande'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function motifDemande()
    {
        return $this->belongsTo('App\Models\MotifDemande', 'id_motif_demande', 'id_motif_demande');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function utilisateur()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'id_utilisateur', 'id_utilisateur');
    }
}
