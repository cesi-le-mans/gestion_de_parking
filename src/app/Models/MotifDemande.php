<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_motif_demande
 * @property string $libelle_motif_demande
 * @property Demande[] $demandes
 */
class MotifDemande extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motif_demande';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_motif_demande';

    /**
     * @var array
     */
    protected $fillable = ['id_motif_demande', 'libelle_motif_demande'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function demandes()
    {
        return $this->hasMany('App\Models\Demande', 'id_motif_demande', 'id_motif_demande');
    }
}
