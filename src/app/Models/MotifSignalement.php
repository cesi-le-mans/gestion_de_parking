<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_motif_signalement
 * @property string $libelle_motif_signalement
 */
class MotifSignalement extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motif_signalement';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_motif_signalement';

    /**
     * @var array
     */
    protected $fillable = ['libelle_motif_signalement'];

}
