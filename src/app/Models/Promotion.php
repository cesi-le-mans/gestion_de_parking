<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $code_promotion
 * @property int $id_formation
 * @property int $id_utilisateur_referent
 * @property string $date_debut
 * @property string $date_fin
 * @property Formation $formation
 * @property Utilisateur $utilisateur
 * @property Utilisateur[] $utilisateurs
 */
class Promotion extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promotion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'code_promotion';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['code_promotion' ,'id_formation', 'id_utilisateur_referent', 'date_debut', 'date_fin'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formation()
    {
        return $this->belongsTo('App\Models\Formation', 'id_formation', 'id_formation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function utilisateur()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'id_utilisateur_referent', 'id_utilisateur');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function utilisateurs()
    {
        return $this->hasMany('App\Models\Utilisateur', 'code_promotion', 'code_promotion');
    }
}
