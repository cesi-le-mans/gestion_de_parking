<?php


namespace App\Models;


class Constantes
{
    const ID_MOTIF_DEMANDE_HANDICAPE = 1;
    const ID_MOTIF_ELEVATION_REFERENT = 2;
    const ID_MOTIF_ELEVATION_ADMIN = 3;
    const ID_ROLE_ADMIN = 1;
    const ID_ROLE_ETUDIANT = 2;
    const ID_ROLE_REFERENT = 3;
}
