<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_typologie_vehicule
 * @property string $typologie_vehicule
 * @property Vehicule[] $vehicules
 */
class Typologie extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'typologie';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_typologie_vehicule';

    /**
     * @var array
     */
    protected $fillable = ['typologie_vehicule'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicules()
    {
        return $this->hasMany('App\Models\Vehicule', 'id_typologie_vehicule', 'id_typologie_vehicule');
    }
}
