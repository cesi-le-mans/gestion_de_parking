<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $immatriculation
 * @property int $id_utilisateur
 * @property int $id_type_vehicule
 * @property int $id_modele
 * @property int $id_typologie_vehicule
 * @property string $couleur
 * @property Modele $modele
 * @property Type $type
 * @property Typologie $typologie
 * @property Utilisateur $utilisateur
 */
class Vehicule extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vehicule';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'immatriculation';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['immatriculation','id_utilisateur', 'id_type_vehicule', 'id_modele', 'id_typologie_vehicule', 'couleur'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modele()
    {
        return $this->belongsTo('App\Models\Modele', 'id_modele', 'id_modele');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Models\Type', 'id_type_vehicule', 'id_type_vehicule');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typologie()
    {
        return $this->belongsTo('App\Models\Typologie', 'id_typologie_vehicule', 'id_typologie_vehicule');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function utilisateur()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'id_utilisateur', 'id_utilisateur');
    }
}
