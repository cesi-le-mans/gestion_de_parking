<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id_signalement
 * @property int $id_utilisateur
 * @property string $immatriculation
 * @property int $id_motif_signalement
 * @property string $date_signalement
 * @property string $signalement_image
 * @property Utilisateur $utilisateur
 * @property Vehicule $vehicule
 * @property MotifSignalement $motifSignalement
 * @property boolean $signalement_traite
 */
class Signalement extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'signalement';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_signalement';

    /**
     * @var array
     */
    protected $fillable = ['id_utilisateur', 'immatriculation', 'id_motif_signalement', 'date_signalement', 'signalement_image', 'signalement_traite'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function utilisateur()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'id_utilisateur', 'id_utilisateur');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicule()
    {
        return $this->belongsTo('App\Models\Vehicule', 'immatriculation', 'immatriculation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function motifSignalement()
    {
        return $this->belongsTo('App\Models\MotifSignalement', 'id_motif_signalement', 'id_motif_signalement');
    }

}
