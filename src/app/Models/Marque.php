<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_marque
 * @property string $nom_marque
 * @property Modele[] $modeles
 * @property Vehicule[] $vehicules
 */
class Marque extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'marque';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_marque';

    /**
     * @var array
     */
    protected $fillable = ['nom_marque'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function modeles()
    {
        return $this->hasMany('App\Models\Modele', 'id_marque', 'id_marque');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicules()
    {
        return $this->hasMany('App\Models\Vehicule', 'id_marque', 'id_marque');
    }
}
