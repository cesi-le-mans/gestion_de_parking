<?php

namespace App\Imports;

use App\Models\Utilisateur;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UtilisateurImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!Utilisateur::where('email', $row[1])->first() ) {
            return new Utilisateur([
                'email'     => $row[1],
                'nom'       => $row[2],
                'prenom'    => $row[3],
                'numero_telephone'  => $row[4],
                'password' => Hash::make(Str::random(12)),
                'utilisateur_handicape'  => $row[5] ?? null,
                'email_verified_at'  => $row[6] ?? now(),
                'id_role'  => $row[7] ?? 2,
                'code_promotion'  => $row[8] ?? null,
            ]);
        } else {
            return null;
        }
        
    }
}
