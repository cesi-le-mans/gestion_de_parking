<?php

namespace App\Imports;

use App\Models\Modele;
use App\Models\Marque;
use Maatwebsite\Excel\Concerns\ToModel;

class ModeleImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $marque = Marque::where('nom_marque', $row[3])->first();
        if ($marque) {
            return new Modele([
                'nom_modele'     => $row[1],
                'id_marque'       => $marque->id_marque,
            ]);
        } else {
            return null;
        }
        
    }
}
