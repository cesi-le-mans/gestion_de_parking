<?php

namespace App\Imports;

use App\Models\Marque;
use Maatwebsite\Excel\Concerns\ToModel;

class MarqueImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!Marque::where('nom_marque', $row[1])->first() ) {
            return new Marque([
                'nom_marque'     => $row[1],
            ]);
        } else {
            return null;
        }
        
    }
}
