<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Signalement de véhicule - Gestion de parking CESI</h2>
<p>Un véhicule dont vous êtes le propriétait fait l'objet d'un signalement</p>
<p>Informations sur le signalement :</p>
<ul>
    <li><strong>Vehicule concerné</strong> : {{ $data['immatriculation'] }} {{ $data['marque'] }} {{ $data['modele'] }}</li>
    <li><strong>Classe de l'apprenant</strong> : {{ $data['classe'] }}</li>
    <li><strong>Motif de signalement du vehicule</strong> : {{ $data['motif'] }}</li>
</ul>

<p>NB : si une photo était jointe au signalement, celle-ci est en pièce-jointe.</p>
</body>
</html>

