@extends('layouts.utilisateur')

@section('main')
    <div id="userView">
    <div class="row">
        <div id="vehicules" class="col-md-6">
            <div class="card">
                <div class="card-header border-0">
                    <h3 class="card-title">Mes véhicules</h3>
                </div>
                <div class="card-body table-responsive p-0" v-if="vehicules.length > 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr v-for="vehicule in filteredVehicules">
                            <td>
                                <i class="fas fa-car"></i>
                                @{{ vehicule.modele.marque.nom_marque ?? ""}} @{{ vehicule.modele.nom_modele ?? "" }} - Immmatriculation : @{{ vehicule.immatriculation }}
                            </td>
                            <td>
                                <button id='editButton' class='btn btn-success' v-on:click="edition(vehicule)">Edit</button>
                                <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(vehicule)">Delete</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body table-responsive p-0" v-else="vehicules.length == 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr>
                            <td>
                                <i class="fas fa-car"></i>
                                Vous ne possédez aucun véhicule.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveauVehicule">
                    Nouveau véhicule
                </button>
            </div>
        </div>

        <div id="demandes" class="col-md-6">
            <div class="card">
                <div class="card-header border-0">
                    <h3 class="card-title">Mes demandes</h3>
                </div>
                <div class="card-body table-responsive p-0" v-if="demandes.length > 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr v-for="(demande) in filteredDemandes">
                            <td>
                                <i class="fas fa-journal-whills"></i>
                                @{{ demande.motif_demande.libelle_motif_demande ?? '' }}
                            </td>
                            <td>
                                <button id='deleteButton' class='btn btn-danger' v-on:click="supprimerDemande(demande)">Delete</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body table-responsive p-0" v-else="demandes.length == 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr>
                            <td>
                                <i class="fas fa-journal-whills"></i>
                                Vous n'avez encore effectué aucune demandes<span></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouvelleDemande">
                    Nouvelle demande
                </button>
            </div>
        </div>
    </div>
        <button type="button" class="btn btn-primary-cesi" v-on:click="nouveauSignalement">
            Signaler un vehicule
        </button>


    <div class="modal fade" id="ajouterEditer" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="resetModifs"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">@{{modalTitle}}</h4>
                </div>
                <div class="modal-body">
                    @include('back.vehicule.vehicule-create-form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="resetModifs">Fermer</button>
                    <button v-if="mode == 'add'"  type="submit" class="btn btn-primary" v-on:click="confAjouter" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    <button v-if="mode == 'edit'"  type="submit" class="btn btn-primary" v-on:click="confModifier" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ajoutDemande" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">@{{modalTitle}}</h4>
                </div>
                <div class="modal-body">
                    @include('back.demande.demande-create-form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="resetModifs">Fermer</button>
                    <button type="submit" class="btn btn-primary" v-on:click="confAjouterDemande" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ajoutSignalement" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">@{{modalTitle}}</h4>
                </div>
                <div class="modal-body">
                    @include('back.signalement.signalement-create-form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="resetModifs">Fermer</button>
                    <button type="submit" class="btn btn-primary" v-on:click="confAjouterSignalement" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                </div>
            </div>
        </div>
    </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center">@{{modalTitle}}</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le véhicule  <span class="font-weight-bold" >@{{selectedVehicule.immatriculation}} ?</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                            <button class="btn btn-success" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="deleteDemande" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center">@{{modalTitle}}</h4>
                    </div>
                    <form id="deleteFormDemande" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer la demande ayant comme motif : <span class="font-weight-bold" >@{{selectedDemande.motif_demande.libelle_motif_demande}} ?</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                            <button class="btn btn-success" v-on:click="confSupprimerDemande" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>






    <script>
        window.onload = function () {

            var vehicule = new Vue({
                el: '#userView',
                data: {
                    filter:'',
                    vehicules: {!! json_encode($vehicules) !!},
                    types: {!! json_encode($types) !!},
                    marques: {!! json_encode($marques) !!},
                    modeles: {!! json_encode($modeles) !!},
                    typologies: {!! json_encode($typologies) !!},
                    utilisateurs: {!! json_encode($utilisateurs) !!},
                    demandes: {!! json_encode($demandes) !!},
                    motifsDemande: {!! json_encode($motifsDemande) !!},
                    motifsSignalement: {!! json_encode($motifsSignalement) !!},
                    modalTitle: "",
                    mode: "",
                    selectedVehicule: { modele: {marque: {} }, type: {}, typologie: {}, utilisateur: {} },
                    selectedDemande: { motif_demande: {}},
                    signalement: { motif_signalement: {}},
                    vehiculeCached: {},
                    buttonDisabled: false,
                    adminView: false,
                },
                mounted: function(){
                    data: {

                    }
                },
                computed: {
                    filteredVehicules() {
                        return this.vehicules.filter(vehicule => {
                                return vehicule.id_utilisateur == {{ Auth::user()->getAuthIdentifier()}}
                        });
                    },
                    filteredDemandes() {
                        return this.demandes.filter(demande => {
                            return demande.id_utilisateur == {{ Auth::user()->getAuthIdentifier()}}
                        });
                    }
                },

                methods: {
                    nouveauVehicule: function(){
                        this.selectedVehicule = { modele: {marque: {} }, type: {}, typologie: {}, utilisateur: {} };
                        this.modalTitle = "Ajouter un nouveau véhicule";
                        this.mode = "add";
                        $('#ajouterEditer').modal('show');
                    },
                    edition: function(vehicule) {
                        //On map le véhicule sur lequel on a cliqué au véhicule selectionné
                        this.selectedVehicule = vehicule;

                        //On fait une copie de le véhicule sélectionné avant modification
                        this.vehiculeCached = Object.assign({}, vehicule);
                        this.modalTitle = "Edition du véhicule " + vehicule.immatriculation + " " +vehicule.modele.nom_modele;
                        this.mode = "edit";

                        $('#ajouterEditer').modal('show');
                    },
                    supprimer: function (vehicule){
                        this.selectedVehicule = vehicule;
                        $('#delete').modal('show');
                        this.modalTitle = "Suppression du véhicule " + vehicule.immatriculation + " " +vehicule.modele;
                    },
                    confAjouter: function(){

                        this.buttonDisabled = true;
                        let postData = {
                            immatriculation: this.selectedVehicule.immatriculation ,
                            couleur: this.selectedVehicule.couleur ,
                            id_utilisateur: {{Auth::user()->getAuthIdentifier()}} ,
                            id_type_vehicule: this.selectedVehicule.type.id_type_vehicule ,
                            id_modele: this.selectedVehicule.modele.id_modele,
                            id_typologie_vehicule: this.selectedVehicule.typologie.id_typologie_vehicule,
                        }
                        axios
                            .post('http://localhost:8080/api/vehicule', postData)
                            .then(response => {
                                this.selectedVehicule.id_utilisateur = {{Auth::user()->getAuthIdentifier()}}
                                this.vehicules.push(this.selectedVehicule);

                                fireToast('success', "Le vehicule " + this.selectedVehicule.immatriculation + " à été ajouté avec succès");
                            })
                            .catch(error => {
                                fireToast('error', "Erreur lors de l'ajout du vehicule'");
                                console.log(error);
                            })
                            .finally(() =>  {
                                    this.buttonDisabled = false;
                                    $('#ajouterEditer').modal('hide');
                                }
                            );
                    },
                    confModifier: function() {
                        this.buttonDisabled = true;
                        let data = {
                            immatriculation: this.selectedVehicule.immatriculation ,
                            couleur: this.selectedVehicule.couleur ,
                            id_utilisateur: this.selectedVehicule.utilisateur.id_utilisateur ,
                            id_type_vehicule: this.selectedVehicule.type.id_type_vehicule ,
                            id_modele: this.selectedVehicule.modele.id_modele,
                            id_typologie_vehicule: this.selectedVehicule.typologie.id_typologie_vehicule,
                        }

                        axios
                            .patch('http://localhost:8080/api/vehicule/' + this.vehiculeCached.immatriculation, data)
                            .then(response => {
                                fireToast('success', "Le véhicule " + this.selectedVehicule.immatriculation + " à été édité avec succès");
                            })
                            .catch(error => {
                                fireToast('error', "Erreur lors de la modification du véhicule ");
                            })
                            .finally( () =>  {
                                    this.buttonDisabled = false;
                                    $('#ajouterEditer').modal('hide');
                                }
                            );
                    },

                    confSupprimer: function(){
                        this.buttonDisabled = true;
                        axios
                            .delete('http://localhost:8080/api/vehicule/' + this.selectedVehicule.immatriculation)
                            .then(response => {
                                this.vehicules = this.vehicules.filter(v => v.immatriculation !== this.selectedVehicule.immatriculation);
                                fireToast('success', "Le véhicule " + this.selectedVehicule.immatriculation + " à été supprimé avec succès");
                            })
                            .catch(error => {
                                fireToast('error', "Erreur lors de la suppression du véhicule ");
                            })
                            .finally( () =>  {
                                    this.buttonDisabled = false;
                                    $('#delete').modal('hide');
                                }
                            );
                    },
                    resetModifs: function(){
                        //on réassigne le véhicule mis en cache si on annule les modifs
                        Object.assign(this.selectedVehicule,this.vehiculeCached);
                    },
                    nouvelleDemande: function(){
                        this.selectedDemande = { motif_demande: {}};
                        this.modalTitle = "Effectuer une demande";
                        this.mode = "add";
                        $('#ajoutDemande').modal('show');
                    },
                    confAjouterDemande: function(){

                        this.buttonDisabled = true;
                        let postData = {
                            commentaire_demande: this.selectedDemande.commentaire_demande,
                            id_utilisateur: {{Auth::user()->getAuthIdentifier()}},
                            id_motif_demande: this.selectedDemande.motif_demande.id_motif_demande,
                        }
                        axios
                            .post('http://localhost:8080/api/demande', postData)
                            .then(response => {
                                console.log(this.selectedDemande)
                                this.selectedDemande.id_demande = response.data.id_demande;
                                this.selectedDemande.id_utilisateur = {{Auth::user()->getAuthIdentifier()}};
                                this.demandes.push(this.selectedDemande);
                                fireToast('success', "La demande à été ajoutée avec succès");
                            })
                            .catch(error => {
                                fireToast('error', "Erreur lors de l'ajout de la demande'");
                                console.log(error);
                            })
                            .finally(() =>  {
                                    this.buttonDisabled = false;
                                    $('#ajoutDemande').modal('hide');
                                }
                            );
                    },
                    supprimerDemande: function(demande){
                        this.selectedDemande = demande;
                        $('#deleteDemande').modal('show');
                        this.modalTitle = "Suppression de la demande " + this.selectedDemande.motif_demande.libelle_motif_demande;
                    },
                    confSupprimerDemande: function(){
                        this.buttonDisabled = true;
                        axios
                            .delete('http://localhost:8080/api/demande/' + this.selectedDemande.id_demande)
                            .then(response => {
                                this.demandes = this.demandes.filter(d => d.id_demande !== this.selectedDemande.id_demande);
                                fireToast('success', "La demande à été supprimé avec succès");
                            })
                            .catch(error => {
                                fireToast('error', "Erreur lors de la suppression de la demande");
                            })
                            .finally( () =>  {
                                    this.buttonDisabled = false;
                                    $('#deleteDemande').modal('hide');
                                }
                            );
                    },
                    nouveauSignalement: function(){
                        this.signalement = { motif_signalement: {}};
                        this.modalTitle = "Effectuer un signalement";
                        $('.filter-option-inner-inner')[0].innerHTML = "Choisissez un vehicule";

                        bsCustomFileInput.init();


                        $('#ajoutSignalement').modal('show');
                        $('#customFile').val('');
                        $('#customFile').next('label').html('Choisissez une image');


                    },
                    confAjouterSignalement: function(){
                        let data = new FormData();
                        this.signalement.id_utilisateur = {{ Auth::user()->getAuthIdentifier()}};

                        data.append('signalement_image', this.signalement.signalement_image);
                        data.append('id_utilisateur', this.signalement.id_utilisateur);
                        data.append('immatriculation', this.signalement.immatriculation);
                        data.append('id_motif_signalement', this.signalement.motif_signalement.id_motif_signalement);
                        data.append('nom_utilisateur', (this.utilisateurs.filter(u => {
                            return u.id_utilisateur == {{ Auth::user()->getAuthIdentifier()}}
                        })[0].nom));
                        axios.post('http://localhost:8080/api/signalement', data)
                        .then(response => {
                            if(response.data == 208){
                                fireToast('info', "Un signalement existe déjà pour ce véhicule");
                            }
                            else{
                                fireToast('success', "Le vehicule à bien été signalé");
                            }
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors du signalement du véhicule");
                        })
                        .finally(() =>{
                            $('#ajoutSignalement').modal('hide');
                        })




                    },
                    uploadFile: function(){
                        this.signalement.signalement_image = document.querySelector('#file').files[0];
                        console.log('on a upload un fichier');
                        console.log(this.signalement.signalement_image);
                    }
                },
            })}
    </script>

@endsection
