@extends('back.layout')

@section('main')
<div id="utilisateur">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Gestion des utilisateurs</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body col-md-12">
            <div class="text-right">
                <label for="recherche">Rechercher</label>
                <input type="text" name="recherche" placeholder="Rechercher" v-model="filter" />
            </div>

            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Mail</th>
                        <th>Téléphone</th>
                        <th>Handicapé</th>
                        <th>Promotion</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                    <tr v-for="(utilisateur) in filteredRows">
                        <td>@{{ utilisateur.nom }}</td>
                        <td>@{{ utilisateur.prenom }}</td>
                        <td>@{{ utilisateur.email }}</td>
                        <td>@{{ utilisateur.numero_telephone }}</td>
                        <td>@{{ utilisateur.utilisateur_handicape == 1 ? 'Oui' : 'Non' }}</td>
                        <td>@{{ utilisateur.code_promotion }}</td>
                        <td>@{{ utilisateur.role == null ? '' : utilisateur.role.nom_role }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(utilisateur)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(utilisateur)">Delete</button>
                        </td>
                    </tr>
                </thead>
            </table>
            <br>
            <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                Créer un utilisateur
            </button>
            
            <button type="button" class="btn btn-primary-cesi" v-on:click="nouveauIE">
                Import/Export
            </button>
        </div>
    </div>

    <!-- Modal ImportExport -->
    <div class="modal fade" id="importExport" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Import / Export</h4>
                </div>
                <div class="modal-body">        
                    <form action="{{ route('utilisateurImport') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" class="form-control">
                        <br>
                        <button class="btn btn-success">Importation des utilisateurs</button>
                        <a class="btn btn-warning" href="{{ route('utilisateurExport') }}">Exportation des utilisateurs</a>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="ajouterEditer" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="resetModifs"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">@{{modalTitle}}</h4>
                </div>
                <div class="modal-body">
                    @include('back.utilisateur.user-create-form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="resetModifs">Fermer</button>
                    <button v-if="mode == 'add'" type="submit" class="btn btn-primary" v-on:click="confAjouter" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    <button v-if="mode == 'edit'" type="submit" class="btn btn-primary" v-on:click="confModifier" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center">@{{modalTitle}}</h4>
                </div>
                <form id="deleteForm" action="#">
                    <div class="modal-body">
                        <p class="text-center">
                            Voulez-vous supprimer l'utilisateur <span class="font-weight-bold">@{{selectedUser.nom}} @{{ selectedUser.prenom }} ?</span>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                        <button class="btn btn-success" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection


<script>
    window.onload = function() {
        var utilisateur = new Vue({
            el: '#utilisateur',
            data: {
                filter: '',
                utilisateurs: {!! json_encode($utilisateurs) !!},
                roles: {!!json_encode($roles) !!},
                promotions: {!!json_encode($promotions) !!},
                modalTitle: "",
                mode: "",
                selectedUser: {
                    role: {},
                    promotion: {}
                },
                utilisateurCached: {},
                buttonDisabled: false,
            },

            mounted: function() {
                data: {

                }
            },
            computed: {
                filteredRows() {
                    return this.utilisateurs.filter(utilisateur => {
                        const nom = utilisateur.nom.toString().toLowerCase();
                        const searchTerm = this.filter.toLowerCase();

                        return nom.includes(searchTerm);
                    });
                }
            },

            methods: {
                nouveau: function() {
                    this.selectedUser = {
                        role: {},
                        promotion: {}
                    };
                    this.modalTitle = "Ajouter une nouvel utilisateur";
                    this.mode = "add";
                    $('#ajouterEditer').modal('show');
                },
                nouveauIE: function() {
                    $('#importExport').modal('show');
                },
                edition: function(utilisateur) {
                    //On map l'utilisateur sur lequel on a cliqué à l'utilisateur selectionné
                    this.selectedUser = utilisateur;

                    //On fait une copie de l'utilisateur sélectionné avant modification
                    this.utilisateurCached = Object.assign({}, utilisateur);
                    this.modalTitle = "Edition de l'utilisateur " + utilisateur.nom + " " + utilisateur.prenom;
                    this.mode = "edit";

                    $('#ajouterEditer').modal('show');
                },
                supprimer: function(utilisateur) {
                    this.selectedUser = utilisateur;
                    $('#delete').modal('show');
                    this.modalTitle = "Suppression de la promotion " + utilisateur.nom + " " + utilisateur.prenom;
                },

                confAjouter: function() {
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/utilisateur', this.selectedUser)
                        .then(response => {
                            this.selectedUser.id_utilisateur = response.data.id;
                            this.utilisateurs.push(this.selectedUser);
                            fireToast('success', "L'utilisateur " + this.selectedUser.nom + " " + this.selectedUser.prenom + " à été ajoutée avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout de l'utilisateur");
                        })
                        .finally(() => {
                            this.buttonDisabled = false;
                            $('#ajouterEditer').modal('hide');
                        });
                },
                confModifier: function() {
                    console.log(this.selectedUser);
                    this.buttonDisabled = true;
                    let data = {
                        nom: this.selectedUser.nom,
                        prenom: this.selectedUser.prenom,
                        email: this.selectedUser.email,
                        id_role: this.selectedUser.id_role,
                        utilisateur_handicape: this.selectedUser.utilisateur_handicape,
                        code_promotion: this.selectedUser.code_promotion,
                        telephone: this.selectedUser.telephone,
                    }
                    axios
                        .patch('http://localhost:8080/api/utilisateur/' + this.utilisateurCached.id_utilisateur, data)
                        .then(response => {
                            fireToast('success', "L'utilisateur " + this.selectedUser.nom + " " + this.selectedUser.prenom + " à été édité avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification de l'utilisateur ");
                        })
                        .finally(() => {
                            this.buttonDisabled = false;
                            $('#ajouterEditer').modal('hide');
                        });
                },

                confSupprimer: function() {
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/utilisateur/' + this.selectedUser.id_utilisateur)
                        .then(response => {
                            this.utilisateurs = this.utilisateurs.filter(u => u.id_utilisateur !== this.selectedUser.id_utilisateur);
                            fireToast('success', "L'utilisateur " + this.selectedUser.nom + " " + this.selectedUser.prenom + " à été supprimé avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de l'utilisateur");
                            console.log(error);
                        })
                        .finally(() => {
                            this.buttonDisabled = false;
                            $('#delete').modal('hide');
                        });
                },
                onChangeRole: function() {
                    this.selectedUser.id_role = this.selectedUser.role.id_role;
                },
                onChangePromotion: function() {
                    this.selectedUser.code_promotion = this.selectedUser.promotion.code_promotion;
                },
                resetModifs: function() {
                    //on réassigne l'utilisateur mis en cache si on annule les modifs
                    Object.assign(this.selectedUser, this.utilisateurCached);
                },
            }

        })
    }
</script>