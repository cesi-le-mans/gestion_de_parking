@extends('back.layout')

@section('main')
    <div id="formation">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Gestion des formations</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body col-md-12">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Nom formation</th>
                    <th>Sigle formation</th>
                    <th>Action</th>
                </tr>
                <tr v-for="(formation, i) in formations">
                    <td>@{{  formation.nom_formation }}</td>
                    <td>@{{  formation.sigle_formation }}</td>
                    <td>
                        <button id='editButton' class='btn btn-info' v-on:click="edition(formation, i)">Edit</button>
                        <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                    </td>
                </tr>
                </thead>
            </table>
            <br>
            <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                Ajouter une formation
            </button>
        </div>
    </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                        <div class="modal-body">
                            @include('back.utilisateur.formation-create-form')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                        </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer la formation  <span class="font-weight-bold" >@{{delInfo.nom_formation}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var formation = new Vue({
            el: '#formation',
            data: {
                formations: {!! json_encode($formations) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idFormation: "",
                    nomFormation: "",
                    sigleFormation: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouvelle formation";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idFormation: "",
                        nomFormation: "",
                        sigleFormation: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                edition: function(formation, key) {
                    this.modalTitle = "Edition formation";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idFormation: formation.id_formation,
                        nomFormation: formation.nom_formation,
                        sigleFormation: formation.sigle_formation,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.formations[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.formations[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/formation/' + this.delInfo.id_formation )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "La formation " + this.delInfo.nom_formation + "à été supprimé avec succès");
                            this.formations.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                    })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de la formation ");
                    })
                        .finally( () =>  {
                            this.buttonDisabled = false;
                            $('#delete').modal('hide');
                        }
                    );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/formation?nom_formation='+ this.modalAjoutEdit.nomFormation +'&sigle_formation=' + this.modalAjoutEdit.sigleFormation )
                        .then(response => {
                            let tempFormation = response.data;
                            fireToast('success', "La formation " + this.modalAjoutEdit.nomFormation + " à été ajouté");
                            this.formations.push({"id_formation" : tempFormation.id_formation, "nom_formation" : tempFormation.nom_formation, "sigle_formation" : tempFormation.sigle_formation});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout de la formation ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/formation/' + this.modalAjoutEdit.idFormation +'?nom_formation='+ this.modalAjoutEdit.nomFormation +'&sigle_formation=' + this.modalAjoutEdit.sigleFormation )
                        .then(response => {
                            fireToast('success', "La formation " + this.modalAjoutEdit.nomFormation + " à été éditer avec succès");
                            this.formations[this.key].nom_formation = this.modalAjoutEdit.nomFormation;
                            this.formations[this.key].sigle_formation = this.modalAjoutEdit.sigleFormation;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification de la formation ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
        }

    })}

</script>
