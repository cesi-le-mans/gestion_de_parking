
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text"  name="nom" class="form-control" v-model="selectedUser.nom" required>
    </div>
    <div class="form-group">
        <label for="prenom">Prenom</label>
        <input type="text" name="prenom" class="form-control" v-model="selectedUser.prenom" required>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control" v-model="selectedUser.email" required>
    </div>
    <div class="form-group">
        <label v-if="mode == 'add'" for="password">Password </label>
        <input v-if="mode == 'add'" name="password" type="password" class="form-control" v-model="selectedUser.password" required>
    </div>
    <div class="form-group">
        <label for="telephone">Téléphone</label>
        <input type="tel" name="telephone" pattern="[0-9]{10}" class="form-control" v-model="selectedUser.numero_telephone">
    </div>
    <div class="form-group switch-box">
        <label for="handicape">Utilisateur Handicapé</label>
        <input type="checkbox" name="handicape" class="switch_1" true-value=1 false-value=0 v-model="selectedUser.utilisateur_handicape">
    </div>
    <div class="form-group">
        <label for="selectPromotion">Promotion</label>
        <select name="selectPromotion" class="form-control" v-model="selectedUser.promotion.code_promotion" @change="onChangePromotion()" required>
            <option v-for="promotion in promotions" v-bind:value="promotion.code_promotion">
                @{{ promotion.code_promotion }}
            </option>
        </select>
    </div>
    <div class="form-group">
        <label for="selectRole">Role</label>
        <select name="selectRole" class="form-control" v-model="selectedUser.role" @change="onChangeRole()">
            <option v-for="role in roles" v-bind:value="role">
                @{{ role.nom_role }}
            </option>
        </select>
    </div>


