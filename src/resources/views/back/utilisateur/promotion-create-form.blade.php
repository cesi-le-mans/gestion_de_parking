<div class="form-group">
    <label for="code_promotion">Code promotion</label>
    <input type="text" class="form-control" name="code_promotion" id="code_promotion" v-model="selectedPromotion.code_promotion">
    <label for="date_debut">Date debut</label>
    <input type="date" class="form-control" name="date_debut" id="date_debut" v-model="selectedPromotion.date_debut">
    <label for="date_fin">Date fin</label>
    <input type="date" class="form-control" name="date_fin" id="date_fin" v-model="selectedPromotion.date_fin">
    <div class="form-group">
        <label for="selectPromotion">Formation</label>
        <select name="selectPromotion" class="form-control" v-model="selectedPromotion.formation" @change="onChangeFormation()">
            <option v-for="formation in formations" v-bind:value="formation">
                @{{ formation.nom_formation }}
            </option>
        </select>
    </div>

    <div class="form-group">
        <label for="selectedReferent">Référent</label>
        <select name="selectedReferent" class="form-control" v-model="selectedPromotion.utilisateur" @change="onChangeReferent()">
            <option v-for="utilisateur in utilisateurs" v-bind:value="utilisateur">
                @{{ utilisateur.nom }}
            </option>
        </select>
    </div>
</div>
