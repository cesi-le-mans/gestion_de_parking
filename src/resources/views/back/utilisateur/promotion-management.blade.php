@extends('back.layout')

@section('main')
    <div id="promotion">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des promotions</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <div class="text-right">
                    <label for="recherche">Rechercher</label>
                    <input type="text"
                           name="recherche"
                           placeholder="Rechercher"
                           v-model="filter" />
                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Code promotion</th>
                        <th>Date début</th>
                        <th>Date fin</th>
                        <th>Nom de la formation</th>
                        <th>Référent</th>
                        <th>Actions</th>
                    </tr>
                    <tr v-for="(promotion) in filteredRows">
                        <td>@{{ promotion.code_promotion }}</td>
                        <td>@{{ promotion.date_debut ?? ""}}</td>
                        <td>@{{ promotion.date_fin ?? "" }}</td>
                        <td>@{{ promotion.id_formation == null ? '' : promotion.formation.nom_formation }}</td>
                        <td>@{{ promotion.utilisateur == null ? '' : promotion.utilisateur.nom + ' ' + promotion.utilisateur.prenom }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(promotion)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(promotion)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Créer une promotion
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="resetModifs"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.utilisateur.promotion-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="resetModifs" >Fermer</button>
                        <button v-if="mode == 'add'"  type="submit" class="btn btn-primary" v-on:click="confAjouter" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                        <button v-if="mode == 'edit'"  type="submit" class="btn btn-primary" v-on:click="confModifier" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center">@{{modalTitle}}</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer la promotion  <span class="font-weight-bold" >@{{selectedPromotion.code_promotion}} ?</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                            <button class="btn btn-success" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var promotion = new Vue({
            el: '#promotion',
            data: {
                filter:'',
                promotions: {!! json_encode($promotions) !!},
                formations: {!! json_encode($formations) !!},
                utilisateurs: {!! json_encode($utilisateurs) !!},
                modalTitle: "",
                mode: "",
                selectedPromotion: { utilisateur: {}, formation: {} },
                PromotionCached: {},
                buttonDisabled: false,
            },

            mounted: function(){
                data: {

                }
            },
            computed: {
                filteredRows() {
                    return this.promotions.filter(promotion => {
                        const code = promotion.code_promotion.toString().toLowerCase();
                        const searchTerm = this.filter.toLowerCase();

                        return code.includes(searchTerm);
                    });
                }
            },

            methods: {
                nouveau: function(){
                    this.selectedPromotion = { utilisateur: {}, formation: {} };
                    this.modalTitle = "Ajouter une nouvelle promotion";
                    this.mode = "add";
                    $('#ajouterEditer').modal('show');
                },
                edition: function(promotion) {
                    //On map la promotion sur laquelle on a cliqué à la promotion selectionnée
                    this.selectedPromotion = promotion;

                    //On fait une copie de la promotion sélectionnée avant modification
                    this.promotionCached = Object.assign({}, promotion);
                    this.modalTitle = "Edition de la promotion " + promotion.code_promotion;
                    this.mode = "edit";

                    $('#ajouterEditer').modal('show');
                },
                supprimer: function (promotion){
                    this.selectedPromotion = promotion;
                    $('#delete').modal('show');
                    this.modalTitle = "Suppression de la promotion " + promotion.code_promotion;
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    let postData = {
                        code_promotion: this.selectedPromotion.code_promotion,
                        date_debut: this.selectedPromotion.date_debut,
                        date_fin: this.selectedPromotion.date_fin,
                        id_formation: this.selectedPromotion.id_formation,
                        id_utilisateur_referent: this.selectedPromotion.id_utilisateur_referent,
                    }
                    axios
                        .post('http://localhost:8080/api/promotion', postData)
                        .then(response => {
                            this.promotions.push(this.selectedPromotion);
                            fireToast('success', "La promotion " + this.selectedPromotion.code_promotion + " à été ajoutée avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du la promotion'");
                        })
                        .finally(() =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
                confModifier: function() {
                    this.buttonDisabled = true;
                    let data = {
                        code_promotion: this.selectedPromotion.code_promotion,
                        date_debut: this.selectedPromotion.date_debut,
                        date_fin: this.selectedPromotion.date_fin,
                        id_formation: this.selectedPromotion.id_formation,
                        id_utilisateur_referent: this.selectedPromotion.id_utilisateur_referent,
                    }
                    axios
                        .patch('http://localhost:8080/api/promotion/' + this.promotionCached.code_promotion, data)
                        .then(response => {
                            fireToast('success', "La promotion " + this.selectedPromotion.code_promotion + " à été éditée avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification de la promotion ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/promotion/' + this.selectedPromotion.code_promotion )
                        .then(response => {
                            this.promotions = this.promotions.filter(p => p.code_promotion !== this.selectedPromotion.code_promotion);
                            fireToast('success', "La promotion " + this.selectedPromotion.code_promotion + " à été supprimée avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de la promotion");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },
                onChangeFormation: function(){
                    this.selectedPromotion.id_formation = this.selectedPromotion.formation.id_formation;
                },
                onChangeReferent: function(){
                    this.selectedPromotion.id_utilisateur_referent = this.selectedPromotion.utilisateur.id_utilisateur;
                },
                resetModifs: function(){
                    //on réassigne la promotion mis en cache si on annule les modifs
                    Object.assign(this.selectedPromotion,this.promotionCached);
                }
            }

        })}

</script>
