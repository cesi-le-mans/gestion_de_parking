@extends('back.layout')

@section('main')
    <div id="role">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des rôles</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom role</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(role, i) in roles">
                        <td>@{{  role.nom_role }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(role, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Ajouter un rôle
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.utilisateur.role-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le rôle  <span class="font-weight-bold" >@{{delInfo.nom_role}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var role = new Vue({
            el: '#role',
            data: {
                roles: {!! json_encode($roles) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idRole: "",
                    nomRole: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouveau Rôle";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idRole: "",
                        nomRole: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                edition: function(role, key) {
                    this.modalTitle = "Edition rôle";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idRole: role.id_role,
                        nomRole: role.nom_role,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.roles[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.roles[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/role/' + this.delInfo.id_role )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "Le rôle " + this.delInfo.nom_role + " à été supprimé avec succès");
                            this.roles.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression du rôle ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/role?nom_role='+ this.modalAjoutEdit.nomRole)
                        .then(response => {
                            let tempRole = response.data;
                            fireToast('success', "Le rôle " + this.modalAjoutEdit.nomRole + " à été ajouté");
                            this.roles.push({"id_role" : tempRole.id_role, "nom_role" : tempRole.nom_role});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du rôle ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/role/' + this.modalAjoutEdit.idRole +'?nom_role='+ this.modalAjoutEdit.nomRole)
                        .then(response => {
                            fireToast('success', "Le rôle " + this.modalAjoutEdit.nomRole + " à été éditer avec succès");
                            this.roles[this.key].nom_role = this.modalAjoutEdit.nomRole;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification du rôle");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
