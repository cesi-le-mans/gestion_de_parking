<div class="form-group">
    <label>Immatriculation</label>
    <select id="selectImmat" name="selectImmat" class="form-control selectpicker" data-live-search="true" v-model="signalement.immatriculation">
        <option v-for="vehicule in vehicules" v-bind:value="vehicule.immatriculation">
            @{{ vehicule.immatriculation }} - @{{ vehicule.modele.marque.nom_marque }} @{{ vehicule.modele.nom_modele }}
        </option>
    </select>
</div>

<div class="form-group">
    <label>Motif demande</label>
    <select name="selectMotifSignalement" class="form-control" v-model="signalement.motif_signalement">
        <option v-for="motifSignalement in motifsSignalement" v-bind:value="motifSignalement">
            @{{ motifSignalement.libelle_motif_signalement }}
        </option>
    </select>
</div>

<!-- File Button -->
<div class="form-group">
    <label for="exampleInputFile">Photo</label>
    <div class="input-group">
        <div class="custom-file">
            <input id="file" type="file" class="custom-file-input" accept='image/*' v-on:change="uploadFile">
            <label class="custom-file-label" for="exampleInputFile">Choisissez une image</label>
        </div>
    </div>
</div>

