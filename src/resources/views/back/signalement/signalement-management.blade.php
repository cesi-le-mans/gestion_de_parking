@extends('back.layout')

@section('main')
    <div id="signalement">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des signalements</h3>
            </div>
            <!-- /.card-header -->
            <div class="row">
                <div class="card-body col-md-9">

                    <table id="example2" class="table table-bordered table-hover" data-click-to-select="true" data-single-select="true">
                        <thead>
                        <tr>
                            <th>Créateur du signalement</th>
                            <th>Personne et Vehicule signalé</th>
                            <th>Motif signalement</th>
                            <th>Date signalement</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tr v-for="(signalement, i) in filteredSignalements" v-bind:key="signalement.id_signalement"  v-bind:class="{ 'bg-light': i == selectedSignalement }">
                            <td v-on:click="afficherPhoto(signalement, i)">@{{  signalement.utilisateur.nom }} @{{  signalement.utilisateur.prenom }}</td>
                            <td v-on:click="afficherPhoto(signalement, i)">@{{  signalement.vehicule.utilisateur.nom }} @{{  signalement.vehicule.utilisateur.prenom }} - @{{  signalement.immatriculation }} @{{  signalement.vehicule.modele.marque.nom_marque }} @{{  signalement.vehicule.modele.nom_modele }}</td>
                            <td v-on:click="afficherPhoto(signalement, i)">@{{  signalement.motif_signalement.libelle_motif_signalement}}</td>
                            <td v-on:click="afficherPhoto(signalement, i)">@{{  signalement.date_signalement }}</td>

                            <td>
                                <button id='editButton' class='btn btn-info' v-on:click="accepter(signalement)">Accepter</button>
                                <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(signalement.id_signalement)">Supprimer</button>
                            </td>
                        </tr>


                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="card-body col-md-3 d-flex"><img id="preview" class="img-fluid align-self-center w-100"></div>
            </div>
        </div>




    </div>

@endsection


<script>
    window.onload = function () {
        var table = document.getElementById('example2');
        var signalement = new Vue({
            el: '#signalement',
            data: {
                signalements: {!! json_encode($signalements) !!},
                selectedSignalement:0,
                key: "",
                message: "ok",
                delInfo: "",

            },

            mounted: function(){
                data: {

                }
            },
            computed: {
                filteredSignalements() {
                    return this.signalements.filter(s => {
                        return s.signalement_traite == 0
                    });
                }
            },

            methods: {
                accepter: function(signalement){
                    axios.patch('http://localhost:8080/api/signalement/'+signalement.id_signalement)
                        .then(response =>{
                            this.signalements = this.signalements.filter(d => d.id_signalement !== signalement.id_signalement)
                            fireToast('success', "Le signalement à été traité, mails envoyés");
                        })
                },
                supprimer: function(idSignalement) {
                    axios.delete('http://localhost:8080/api/signalement/'+idSignalement)
                        .then(response => {
                            this.signalements = this.signalements.filter(d => d.id_signalement !== idSignalement)
                            document.getElementById('preview').src = " ";
                        })
                },
                afficherPhoto: function(signalement, i){
                    this.selectedSignalement = i;
                    var img = document.getElementById('preview');
                    img.onerror = function() {
                        img.src = "/uploads/images/noimage.png";
                    }

                    if(signalement.signalement_image == null){
                        img.src = "/uploads/images/noimage.png";
                    }
                    else{
                        img.src = signalement.signalement_image;
                    }


                }
            }

        })}

</script>
