@extends('back.layout')

@section('main')
    <div id="motifsSignalement">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des motifs signalements</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom motif signalement</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(motifsSignalement, i) in motifsSignalements">
                        <td>@{{  motifsSignalement.libelle_motif_signalement }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(motifsSignalement, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <button type="button" class="btn btn-primary" v-on:click="nouveau">
                    Ajouter un motif signalement
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.signalement.motifSignalement-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le motif signalement  <span class="font-weight-bold" >@{{delInfo.libelle_motif_signalement}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var motifsSignalement = new Vue({
            el: '#motifsSignalement',
            data: {
                motifsSignalements: {!! json_encode($motifsSignalements) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idMS: "",
                    libelleMS: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouveau motif signalement";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idMS: "",
                        libelleMS: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                edition: function(motifsSignalement, key) {
                    this.modalTitle = "Edition motif signalement";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idMS: motifsSignalement.id_motif_signalement,
                        libelleMS: motifsSignalement.libelle_motif_signalement,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.motifsSignalements[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.motifsSignalements[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/motifSignalement/' + this.delInfo.id_motif_signalement )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "Le motif signalement " + this.delInfo.libelle_motif_signalement + " à été supprimé avec succès");
                            this.motifsSignalements.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression du motif signalement ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    console.log(this.modalAjoutEdit.libelleMS);
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/motifSignalement?libelle_motif_signalement='+ this.modalAjoutEdit.libelleMS)
                        .then(response => {
                            let tempMS = response.data;
                            fireToast('success', "Le motif signalement " + this.modalAjoutEdit.libelleMS + " à été ajouté");
                            this.motifsSignalements.push({"id_motif_signalement" : tempMS.id_motif_signalement, "libelle_motif_signalement" : tempMS.libelle_motif_signalement});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du motif signalement ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/motifSignalement/' + this.modalAjoutEdit.idMS +'?libelle_motif_signalement='+ this.modalAjoutEdit.libelleMS)
                        .then(response => {
                            fireToast('success', "Le motif signalement " + this.modalAjoutEdit.libelleMS + " à été éditer avec succès");
                            this.motifsSignalements[this.key].libelle_motif_signalement = this.modalAjoutEdit.libelleMS;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification du motif signalement");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
