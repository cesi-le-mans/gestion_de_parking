@extends('back.layout')

@section('main')
    <div id="demande">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des demandes</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Motif demande</th>
                        <th>Commentaire demande</th>
                        <th>Utilisateur</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(demande) in demandes">
                        <td>@{{  demande.motif_demande.libelle_motif_demande }}</td>
                        <td>@{{  demande.commentaire_demande }}</td>
                        <td>@{{  demande.utilisateur.nom }} @{{ demande.utilisateur.prenom }}</td>
                        <td>
                            <button id='editButton' class='btn btn-success' v-on:click="valider(demande)">Accepter</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="deleteDemande(demande.id_demande)">Refuser</button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection


<script>
    window.onload = function () {
        var demande = new Vue({
            el: '#demande',
            data: {
                demandes: {!! json_encode($demandes) !!},
            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                valider: function(demande){
                   if(demande.id_motif_demande == {{\App\Models\Constantes::ID_MOTIF_DEMANDE_HANDICAPE}}){
                       var postData = {
                           utilisateur_handicape: 1
                       }
                       axios.patch('http://localhost:8080/api/utilisateur/'+demande.id_utilisateur, postData)
                       .then(response => {
                           fireToast('success', 'La demande à été traitée avec succès')
                           this.deleteDemande(demande.id_demande);
                       })
                       .catch(error => {
                           fireToast("error", 'Erreur lors du traitement de la demande');
                       })
                   }
                   else if(demande.id_motif_demande == {{ \App\Models\Constantes::ID_MOTIF_ELEVATION_REFERENT }}){
                       var postData = {
                           id_role: {{ \App\Models\Constantes::ID_ROLE_REFERENT }}
                       }
                       axios.patch('http://localhost:8080/api/utilisateur/'+demande.id_utilisateur, postData)
                           .then(response => {
                               fireToast('success', 'La demande à été traitée avec succès')
                               this.deleteDemande(demande.id_demande);
                           })
                           .catch(error => {
                               fireToast("error", 'Erreur lors du traitement de la demande');
                           })
                   }
                   else{
                       var postData = {
                           id_role: {{ \App\Models\Constantes::ID_ROLE_ADMIN }}
                       }
                       axios.patch('http://localhost:8080/api/utilisateur/'+demande.id_utilisateur, postData)
                           .then(response => {
                               fireToast('success', 'La demande à été traitée avec succès')
                               this.deleteDemande(demande.id_demande);
                           })
                           .catch(error => {
                               fireToast("error", 'Erreur lors du traitement de la demande');
                           })
                   }

                },
                deleteDemande(idDemande){
                    axios.delete('http://localhost:8080/api/demande/'+idDemande)
                    .then(response => {
                        this.demandes = this.demandes.filter(d => d.id_demande !== idDemande)
                    })
                }
            }
        })}

</script>
