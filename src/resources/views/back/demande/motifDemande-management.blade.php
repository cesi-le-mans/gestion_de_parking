@extends('back.layout')

@section('main')
    <div id="motifsDemande">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des motif demandes</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom motif demande</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(motifsDemande, i) in motifsDemandes">
                        <td>@{{  motifsDemande.libelle_motif_demande }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(motifsDemande, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <button type="button" class="btn btn-primary" v-on:click="nouveau">
                    Ajouter un motif demande
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.demande.motifDemande-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le motif demande  <span class="font-weight-bold" >@{{delInfo.libelle_motif_demande}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var motifsDemande = new Vue({
            el: '#motifsDemande',
            data: {
                motifsDemandes: {!! json_encode($motifsDemandes) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idMD: "",
                    libelleMD: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouveau motif demande";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idMD: "",
                        libelleMD: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                edition: function(motifsDemande, key) {
                    this.modalTitle = "Edition motif demande";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idMD: motifsDemande.id_motif_demande,
                        libelleMD: motifsDemande.libelle_motif_demande,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.motifsDemandes[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.motifsDemandes[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/motifDemande/' + this.delInfo.id_motif_demande )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "Le motif demande " + this.delInfo.libelle_motif_demande + " à été supprimé avec succès");
                            this.motifsDemandes.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression du motif demande ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/motifDemande?libelle_motif_demande='+ this.modalAjoutEdit.libelleMD)
                        .then(response => {
                            let tempMD = response.data;
                            fireToast('success', "Le motif demande " + this.modalAjoutEdit.libelleMD + " à été ajouté");
                            this.motifsDemandes.push({"id_motif_demande" : tempMD.id_motif_demande, "libelle_motif_demande" : tempMD.libelle_motif_demande});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du motif demande ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/motifDemande/' + this.modalAjoutEdit.idMD +'?libelle_motif_demande='+ this.modalAjoutEdit.libelleMD)
                        .then(response => {
                            fireToast('success', "Le motif demande " + this.modalAjoutEdit.libelleMD + " à été édité avec succès");
                            this.motifsDemandes[this.key].libelle_motif_demande = this.modalAjoutEdit.libelleMD;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification du motif demande");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
