<div class="form-group">
    <label>Motif demande</label>
    <select name="selectMotifsDemande" class="form-control" v-model="selectedDemande.motif_demande">
        <option v-for="motifDemande in motifsDemande" v-bind:value="motifDemande">
            @{{ motifDemande.libelle_motif_demande }}
        </option>
    </select>
</div>

<div class="form-group">
    <label>Commentaire demande</label>
    <textarea type="text" class="form-control" v-model="selectedDemande.commentaire_demande"></textarea>
</div>
