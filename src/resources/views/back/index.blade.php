@extends('back.layout')
@section('main')
<div id="app">
    <div class="row">
        <div id="utilisateurs" class="col-md-6 adminBlock">
            <div class="card">

                <div class="card-header border-0">

                    <h3 class="card-title">  <i class="fas fa-user"></i> Derniers utilisateurs inscrits</h3>
                </div>
                <div class="card-body table-responsive p-0" v-if="utilisateurs.length > 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr v-for="utilisateur in utilisateurs">
                            <td>
                                <i class="fas fa-user"></i>
                                @{{ utilisateur.nom }} @{{ utilisateur.prenom }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body table-responsive p-0" v-else="utilisateurs.length == 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr>
                            <td>
                                <i class="fas fa-user"></i>
                                Aucun utilisateur inscrit récemment
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="demandes" class="col-md-6 adminBlock">
            <div class="card">
                <div class="card-header border-0">

                    <h3 class="card-title">  <i class="fas fa-journal-whills"></i> Dernières demandes effectuées</h3>
                </div>
                <div class="card-body table-responsive p-0" v-if="demandes.length > 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr v-for="demande in demandes">
                            <td>
                                <i class="fas fa-journal-whills"></i>
                                @{{ demande.utilisateur.nom ?? ""}} @{{ demande.utilisateur.prenom ?? ""}} - @{{ demande.motif_demande.libelle_motif_demande ?? ""}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body table-responsive p-0" v-else="demandes.length == 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr>
                            <td>
                                <i class="fas fa-journal-whills"></i>
                                Aucune demande récente
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div id="vehicules" class="col-md-6 adminBlock">
            <div class="card">
                <div class="card-header border-0">

                    <h3 class="card-title">  <i class="fas fa-car"></i> Derniers véhicules enregistrés</h3>
                </div>
                <div class="card-body table-responsive p-0" v-if="vehicules.length > 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr v-for="vehicule in vehicules">
                            <td>
                                <i class="fas fa-car"></i>
                                @{{ vehicule.modele.marque.nom_marque ?? ""}} @{{ vehicule.modele.nom_modele ?? "" }} - Immmatriculation : @{{ vehicule.immatriculation }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body table-responsive p-0" v-else="vehicules.length == 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr>
                            <td>
                                <i class="fas fa-car"></i>
                                Aucun vehicule enregistré récemment
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="signalements" class="col-md-6 adminBlock">
            <div class="card">
                <div class="card-header border-0">

                    <h3 class="card-title"> <i class="fas fa-exclamation-circle"></i> Derniers signalements</h3>
                </div>
                <div class="card-body table-responsive p-0" v-if="signalements.length > 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr v-for="signalement in signalements">
                            <td>
                                <i class="fas fa-exclamation-circle"></i>
                                Signalement du vehicule @{{ signalement.immatriculation ?? "" }} @{{ signalement.vehicule.modele.marque.nom_marque }} @{{ signalement.vehicule.modele.nom_modele }} par @{{ signalement.utilisateur.nom }} @{{ signalement.utilisateur.prenom }} - Motif : @{{ signalement.motif_signalement.libelle_motif_signalement }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body table-responsive p-0" v-else="signalements.length == 0">
                    <table class="table table-striped table-valign-middle">
                        <thead>

                        </thead>
                        <tbody class="same-size-row">
                        <tr>
                            <td>
                                <i class="fas fa-exclamation-circle"></i>
                                Aucun signalement récent
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        window.onload = function () {
            var vue = new Vue({
                el: '#app',
                data: {
                    filter:'',
                    vehicules: {!! json_encode($vehicules) !!},
                    demandes: {!! json_encode($demandes) !!},
                    utilisateurs: {!! json_encode($utilisateurs) !!},
                    signalements: {!! json_encode($signalements) !!},
                },
                mounted: function(){
                    data: {

                    }
                },

                methods: {

                },
            })}
    </script>
@endsection
