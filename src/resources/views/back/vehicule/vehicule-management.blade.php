@extends('back.layout')

@section('main')
    <div id="vehicule">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des véhicules</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <div class="text-right">
                    <label for="recherche">Rechercher</label>
                    <input type="text"
                           name="recherche"
                           placeholder="Rechercher"
                           v-model="filter" />
                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Immatriculation</th>
                        <th>Marque</th>
                        <th>Modèle</th>
                        <th>Couleur</th>
                        <th>Type véhicule</th>
                        <th>Typologie véhicule</th>
                        <th>Possesseur véhicule</th>
                        <th>Actions</th>
                    </tr>
                    <tr v-for="(vehicule) in filteredRows">
                        <td>@{{ vehicule.immatriculation }}</td>
                        <td>@{{ vehicule.modele.marque.nom_marque ?? ""}}</td>
                        <td>@{{ vehicule.modele.nom_modele ?? "" }}</td>
                        <td>@{{ vehicule.couleur }}</td>
                        <td>@{{ vehicule.type.type_vehicule }}</td>
                        <td>@{{ vehicule.typologie.typologie_vehicule }}</td>
                        <td>@{{ vehicule.utilisateur.nom }} @{{ vehicule.utilisateur.prenom }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(vehicule)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(vehicule)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Créer un véhicule
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="resetModifs"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.vehicule.vehicule-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="resetModifs">Fermer</button>
                        <button v-if="mode == 'add'"  type="submit" class="btn btn-primary" v-on:click="confAjouter" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                        <button v-if="mode == 'edit'"  type="submit" class="btn btn-primary" v-on:click="confModifier" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center">@{{modalTitle}}</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le véhicule  <span class="font-weight-bold" >@{{selectedVehicule.immatriculation}} ?</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                            <button class="btn btn-success" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var vehicule = new Vue({
            el: '#vehicule',
            data: {
                filter:'',
                vehicules: {!! json_encode($vehicules) !!},
                types: {!! json_encode($types) !!},
                marques: {!! json_encode($marques) !!},
                modeles: {!! json_encode($modeles) !!},
                typologies: {!! json_encode($typologies) !!},
                utilisateurs: {!! json_encode($utilisateurs) !!},
                modalTitle: "",
                mode: "",
                selectedVehicule: { modele: {marque: {} }, type: {}, typologie: {}, utilisateur: {} },
                vehiculeCached: {},
                buttonDisabled: false,
                adminView: true,
            },

            mounted: function(){
                data: {

                }
            },
            computed: {
                filteredRows() {
                    return this.vehicules.filter(vehicule => {
                        const vehicle = vehicule.toString().toLowerCase();
                        const immat = vehicule.immatriculation.toString().toLowerCase();
                        const userLastName = vehicule.utilisateur.nom.toString().toLowerCase();
                        const userFirstName = vehicule.utilisateur.prenom.toString().toLowerCase();
                        const searchTerm = this.filter.toLowerCase();

                        return vehicle.includes(searchTerm) ||
                            immat.includes(searchTerm) ||
                            userLastName.includes(searchTerm) ||
                            userFirstName.includes(searchTerm);
                    });
                }
            },

            methods: {
                nouveau: function(){
                    this.selectedVehicule = { modele: {marque: {} }, type: {}, typologie: {}, utilisateur: {} };
                    this.modalTitle = "Ajouter un nouveau véhicule";
                    this.mode = "add";
                    $('#ajouterEditer').modal('show');
                },
                edition: function(vehicule) {
                    //On map le véhicule sur lequel on a cliqué au véhicule selectionné
                    this.selectedVehicule = vehicule;

                    //On fait une copie de le véhicule sélectionné avant modification
                    this.vehiculeCached = Object.assign({}, vehicule);
                    this.modalTitle = "Edition du véhicule " + vehicule.immatriculation + " " +vehicule.modele.nom_modele;
                    this.mode = "edit";

                    $('#ajouterEditer').modal('show');
                },
                supprimer: function (vehicule){
                    this.selectedVehicule = vehicule;
                    $('#delete').modal('show');
                    this.modalTitle = "Suppression du véhicule " + vehicule.immatriculation + " " +vehicule.modele;
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    let postData = {
                        immatriculation: this.selectedVehicule.immatriculation ,
                        couleur: this.selectedVehicule.couleur ,
                        id_utilisateur: this.selectedVehicule.utilisateur.id_utilisateur ,
                        id_type_vehicule: this.selectedVehicule.type.id_type_vehicule ,
                        id_modele: this.selectedVehicule.modele.id_modele,
                        id_typologie_vehicule: this.selectedVehicule.typologie.id_typologie_vehicule,
                    }
                    axios
                        .post('http://localhost:8080/api/vehicule', postData)
                        .then(response => {
                            this.vehicules.push(this.selectedVehicule);
                            fireToast('success', "Le vehicule " + this.selectedVehicule.immatriculation + " à été ajouté avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du vehicule'");
                        })
                        .finally(() =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
                confModifier: function() {
                    this.buttonDisabled = true;
                    let data = {
                        immatriculation: this.selectedVehicule.immatriculation ,
                        couleur: this.selectedVehicule.couleur ,
                        id_utilisateur: this.selectedVehicule.utilisateur.id_utilisateur ,
                        id_type_vehicule: this.selectedVehicule.type.id_type_vehicule ,
                        id_modele: this.selectedVehicule.modele.id_modele,
                        id_typologie_vehicule: this.selectedVehicule.typologie.id_typologie_vehicule,
                    }

                    axios
                        .patch('http://localhost:8080/api/vehicule/' + this.vehiculeCached.immatriculation, data)
                        .then(response => {
                            fireToast('success', "Le véhicule " + this.selectedVehicule.immatriculation + " à été édité avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification du véhicule ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/vehicule/' + this.selectedVehicule.immatriculation )
                        .then(response => {
                            this.vehicules = this.vehicules.filter(v => v.immatriculation !== this.selectedVehicule.immatriculation);
                            fireToast('success', "Le véhicule " + this.selectedVehicule.immatriculation + " à été supprimé avec succès");
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression du véhicule ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },
                resetModifs: function(){
                    //on réassigne le véhicule mis en cache si on annule les modifs
                    Object.assign(this.selectedVehicule,this.vehiculeCached);
                },
            }

        })}

</script>
