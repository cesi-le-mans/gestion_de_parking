@extends('back.layout')

@section('main')
    <div id="marque">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des marques</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom marques</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(marque, i) in marques">
                        <td>@{{  marque.nom_marque }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(marque, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Ajouter une marque
                </button>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveauIE">
                    Import/Export
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.vehicule.marque-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer la marque  <span class="font-weight-bold" >@{{delInfo.nom_marque}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

                <!-- Modal ImportExport -->
        <div class="modal fade" id="importExport" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Import / Export</h4>
                    </div>
                    <div class="modal-body">        
                        <form action="{{ route('marqueImport') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control">
                            <br>
                            <button class="btn btn-success">Importation des marques</button>
                            <a class="btn btn-warning" href="{{ route('marqueExport') }}">Exportation des marques</a>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var marque = new Vue({
            el: '#marque',
            data: {
                marques: {!! json_encode($marques) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idMarque: "",
                    nomMarque: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouvelle marque";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idMarque: "",
                        nomMarque: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                nouveauIE: function() {
                    $('#importExport').modal('show');
                },
                edition: function(marque, key) {
                    this.modalTitle = "Edition marque";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idMarque: marque.id_marque,
                        nomMarque: marque.nom_marque,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.marques[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.marques[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/marque/' + this.delInfo.id_marque )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "La marque " + this.delInfo.nom_marque + " à été supprimé avec succès");
                            this.marques.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de la marque ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/marque?nom_marque='+ this.modalAjoutEdit.nomMarque)
                        .then(response => {
                            let tempMarque = response.data;
                            fireToast('success', "La marque " + this.modalAjoutEdit.nomMarque + " à été ajouté");
                            this.marques.push({"id_marque" : tempMarque.id_marque, "nom_marque" : tempMarque.nom_marque});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout de la marque ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/marque/' + this.modalAjoutEdit.idMarque +'?nom_marque='+ this.modalAjoutEdit.nomMarque)
                        .then(response => {
                            fireToast('success', "La marque " + this.modalAjoutEdit.nomMarque + " à été éditer avec succès");
                            this.marques[this.key].nom_marque = this.modalAjoutEdit.nomMarque;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification de la marque");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
