<div class="form-group">
    <label>Nom modele</label>
    <input type="text" class="form-control" v-model="modalAjoutEdit.nom_modele">
    <label>Marque</label>
    <select name="selectMarque" class="form-control" v-model="modalAjoutEdit.marque">
        <option v-for="marque in marques" v-bind:value="marque">
            @{{ marque.nom_marque }}
        </option>
    </select>
</div>
