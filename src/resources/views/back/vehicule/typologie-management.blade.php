@extends('back.layout')

@section('main')
    <div id="typologie">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des typologies</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom typologie véhicule</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(typologie, i) in typologies">
                        <td>@{{  typologie.typologie_vehicule }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(typologie, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Ajouter une typologie
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.vehicule.typologie-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer la typologie  <span class="font-weight-bold" >@{{delInfo.typologie_vehicule}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var typologie = new Vue({
            el: '#typologie',
            data: {
                typologies: {!! json_encode($typologies) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idTypo: "",
                    typoVehicule: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouvelle typologie";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idTypo: "",
                        typoVehicule: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                edition: function(typologie, key) {
                    this.modalTitle = "Edition typologie";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idTypo: typologie.id_typologie_vehicule,
                        typoVehicule: typologie.typologie_vehicule,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.typologies[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.typologies[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/typologie/' + this.delInfo.id_typologie_vehicule )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "La typologoe " + this.delInfo.typologie_vehicule + " à été supprimé avec succès");
                            this.typologies.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de la typologie ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/typologie?typologie_vehicule='+ this.modalAjoutEdit.typoVehicule)
                        .then(response => {
                            let tempTypo = response.data;
                            fireToast('success', "La typologie " + this.modalAjoutEdit.typoVehicule + " à été ajouté");
                            this.typologies.push({"id_typologie_vehicule" : tempTypo.id_typologie_vehicule, "typologie_vehicule" : tempTypo.typologie_vehicule});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout de la typologie ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/typologie/' + this.modalAjoutEdit.idTypo +'?typologie_vehicule='+ this.modalAjoutEdit.typoVehicule)
                        .then(response => {
                            fireToast('success', "La typologie " + this.modalAjoutEdit.typoVehicule + " à été éditer avec succès");
                            this.typologies[this.key].typologie_vehicule = this.modalAjoutEdit.typoVehicule;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification de la typologie");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
