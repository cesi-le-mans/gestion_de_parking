@extends('back.layout')

@section('main')
    <div id="type">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des types</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom type</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(type, i) in types">
                        <td>@{{  type.type_vehicule }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(type, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Ajouter un type
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.vehicule.type-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le type  <span class="font-weight-bold" >@{{delInfo.type_vehicule}}</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var type = new Vue({
            el: '#type',
            data: {
                types: {!! json_encode($types) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    idTypeVehicule: "",
                    typeVehicule: "",
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouveau type";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idTypeVehicule: "",
                        typeVehicule: "",
                    }
                    $('#ajouterEditer').modal('show');
                },
                edition: function(type, key) {
                    this.modalTitle = "Edition type";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        idTypeVehicule: type.id_type_vehicule,
                        typeVehicule: type.type_vehicule,
                    }

                    $('#ajouterEditer').modal('show');

                    console.log(this.types[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.types[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    console.log(this.ajoutEdit);
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/type/' + this.delInfo.id_type_vehicule )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "Le type " + this.delInfo.type_vehicule + " à été supprimé avec succès");
                            this.types.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de le type ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/type?type_vehicule='+ this.modalAjoutEdit.typeVehicule)
                        .then(response => {
                            let tempType = response.data;
                            fireToast('success', "La marque " + this.modalAjoutEdit.nomMarque + " à été ajouté");
                            this.types.push({"id_type_vehicule" : tempType.id_type_vehicule, "type_vehicule" : tempType.type_vehicule});
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du type ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    axios
                        .patch('http://localhost:8080/api/type/' + this.modalAjoutEdit.idTypeVehicule +'?type_vehicule='+ this.modalAjoutEdit.typeVehicule)
                        .then(response => {
                            fireToast('success', "Le type " + this.modalAjoutEdit.typeVehicule + " à été éditer avec succès");
                            this.types[this.key].type_vehicule = this.modalAjoutEdit.typeVehicule;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification du type");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
