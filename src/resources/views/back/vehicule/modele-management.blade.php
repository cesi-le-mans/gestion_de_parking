@extends('back.layout')

@section('main')
    <div id="modele">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gestion des modeles</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body col-md-12">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom modele</th>
                        <th>Nom marque</th>
                        <th>Action</th>
                    </tr>
                    <tr v-for="(modele, i) in modeles">
                        <td>@{{  modele.nom_modele }}</td>
                        <td>@{{  modele.marque.nom_marque }}</td>
                        <td>
                            <button id='editButton' class='btn btn-info' v-on:click="edition(modele, i)">Edit</button>
                            <button id='deleteButton' class='btn btn-danger' v-on:click="supprimer(i)">Delete</button>
                        </td>
                    </tr>
                    </thead>
                </table>
                <br>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveau">
                    Ajouter un modele
                </button>
                <button type="button" class="btn btn-primary-cesi" v-on:click="nouveauIE">
                    Import/Export
                </button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="ajouterEditer" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@{{modalTitle}}</h4>
                    </div>
                    <div class="modal-body">
                        @include('back.vehicule.modele-create-form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        <button  class="btn btn-primary" v-on:click="ajoutOrEdit" v-bind:disabled="buttonDisabled">Sauvegarder</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Suppression d'un enregistrement</h4>
                    </div>
                    <form id="deleteForm" action="#">
                        <div class="modal-body">
                            <p class="text-center">
                                Voulez-vous supprimer le modele  <span class="font-weight-bold" >@{{delInfo.nom_modele}}</span> ?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button class="btn btn-warning" v-on:click="confSupprimer" v-bind:disabled="buttonDisabled">Oui</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

            <!-- Modal ImportExport -->
        <div class="modal fade" id="importExport" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Import / Export</h4>
                    </div>
                    <div class="modal-body">        
                        <form action="{{ route('modeleImport') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control">
                            <br>
                            <button class="btn btn-success">Importation des modèles</button>
                            <a class="btn btn-warning" href="{{ route('modeleExport') }}">Exportation des modèles</a>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


<script>
    window.onload = function () {
        var modele = new Vue({
            el: '#modele',
            data: {
                modeles: {!! json_encode($modeles) !!},
                marques: {!! json_encode($marques) !!},
                key: "",
                message: "ok",
                delInfo: "",
                modalTitle: "",
                ajoutEdit: "",
                modalAjoutEdit: {
                    id_modele: "",
                    nom_modele: "",
                    marque: { id_marque : "", nom_marque : ""}
                },
                buttonDisabled: false,

            },

            mounted: function(){
                data: {

                }
            },

            methods: {
                nouveau: function(){
                    this.modalTitle = "Nouveau modeles";
                    this.ajoutEdit = "add";
                    this.modalAjoutEdit = {
                        idModele: "",
                            nomModele: "",
                            marque: { id_marque : "", nom_marque : ""}
                    },
                    $('#ajouterEditer').modal('show');
                },
                nouveauIE: function() {
                    $('#importExport').modal('show');
                },
                edition: function(modele, key) {
                    this.modalTitle = "Edition modele";
                    this.ajoutEdit = "edit";
                    this.key = key;
                    this.modalAjoutEdit = {
                        id_modele: modele.id_modele,
                        nom_modele: modele.nom_modele,
                        marque: { id_marque : modele.marque.id_marque, nom_marque : modele.marque.nom_marque },
                    }
                    $('#ajouterEditer').modal('show');

                    console.log(this.modeles[key]);

                },
                supprimer: function (key){
                    this.delInfo = this.modeles[key];
                    this.key = key;
                    $('#delete').modal('show');
                },

                ajoutOrEdit: function()
                {
                    if(this.ajoutEdit == "add"){
                        this.confAjouter();
                    }
                    else{
                        this.confModifier();
                    };
                },

                confSupprimer: function(){
                    this.buttonDisabled = true;
                    axios
                        .delete('http://localhost:8080/api/modele/' + this.delInfo.id_modele )
                        .then(response => {
                            console.log("OK" + response);
                            fireToast('success', "Le modele " + this.delInfo.nom_modele + " à été supprimé avec succès");
                            this.modeles.splice(this.key, 1);
                            console.log(this.buttonDisabled);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la suppression de le modele ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#delete').modal('hide');
                            }
                        );
                },

                confAjouter: function(){
                    this.buttonDisabled = true;
                    axios
                        .post('http://localhost:8080/api/modele?nom_modele='+ this.modalAjoutEdit.nom_modele+'&id_marque=' + this.modalAjoutEdit.marque.id_marque)
                        .then(response => {
                            let tempModele = response.data;
                            tempModele.marque = this.modalAjoutEdit.marque;
                            console.log (tempModele);
                            fireToast('success', "La marque " + this.modalAjoutEdit.nom_modele + " à été ajouté");
                            this.modeles.push(tempModele);
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de l'ajout du modele ");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },

                confModifier: function() {
                    this.buttonDisabled = true;
                    console.log(this.modalAjoutEdit);
                    axios
                        .patch('http://localhost:8080/api/modele/' + this.modalAjoutEdit.id_modele +
                            '?nom_modele='+ this.modalAjoutEdit.nom_modele + '&id_marque=' + this.modalAjoutEdit.marque.id_marque)
                        .then(response => {
                            fireToast('success', "Le modele " + this.modalAjoutEdit.modele + " à été éditer avec succès");
                            this.modeles[this.key] = this.modalAjoutEdit;
                        })
                        .catch(error => {
                            fireToast('error', "Erreur lors de la modification du modele");
                        })
                        .finally( () =>  {
                                this.buttonDisabled = false;
                                $('#ajouterEditer').modal('hide');
                            }
                        );
                },
            }

        })}

</script>
