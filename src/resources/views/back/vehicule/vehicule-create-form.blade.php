<div class="form-group">
    <label for="immatriculation">Immatriculation</label>
    <input type="text"  name="immatriculation" class="form-control" v-model="selectedVehicule.immatriculation">

    <label for="couleur">Couleur</label>
    <input type="text" name="couleur" class="form-control" v-model="selectedVehicule.couleur">

    <label for="selectMarque">Marque</label>
    <select name="selectMarque" class="form-control" v-model="selectedVehicule.modele.marque">
        <option v-for="marque in marques" v-bind:value="marque">
            @{{ marque.nom_marque }}
        </option>
    </select>

    <label for="selectModele">Modele</label>
    <select name="selectModele" class="form-control" v-model="selectedVehicule.modele" v-bind:disabled="selectedVehicule.modele.marque.id_marque == null">
        <option v-for="modele in modeles" v-bind:value="modele" v-if="selectedVehicule.modele.marque.id_marque == modele.id_marque">
            @{{ modele.nom_modele }}
        </option>
    </select>

    <label for="selectTypeVehicule">Type véhicule</label>
    <select name="selectModele" class="form-control" v-model="selectedVehicule.type">
        <option v-for="type in types" v-bind:value="type">
            @{{ type.type_vehicule }}
        </option>
    </select>

    <label for="selectTypologieVehicule">Typologie véhicule</label>
    <select name="selectModele" class="form-control" v-model="selectedVehicule.typologie">
        <option v-for="typologie in typologies" v-bind:value="typologie">
            @{{ typologie.typologie_vehicule }}
        </option>
    </select>
    <div v-if="adminView">
    <label for="selectPossesseurVehicule">Possesseur véhicule</label>
    <select name="selectModele" class="form-control" v-model="selectedVehicule.utilisateur">
        <option v-for="utilisateur in utilisateurs" v-bind:value="utilisateur">
            @{{ utilisateur.nom }} @{{ utilisateur.prenom }}
        </option>
    </select>
    </div>
</div>

