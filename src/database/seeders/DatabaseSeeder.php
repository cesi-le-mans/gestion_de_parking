<?php

namespace Database\Seeders;

use App\Models\Constantes;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* INSERTION DES ROLES */
        DB::table('role')->insert([
            'id_role' => Constantes::ID_ROLE_ADMIN,
            'nom_role' => "ROLE_ADMIN",
        ]);

        DB::table('role')->insert([
            'id_role' => Constantes::ID_ROLE_ETUDIANT,
            'nom_role' => "ROLE_ETUDIANT",
        ]);

        DB::table('role')->insert([
            'id_role' => Constantes::ID_ROLE_REFERENT,
            'nom_role' => "ROLE_REFERENT",
        ]);

        /* INSERTION MOTIF DEMANDES */
        DB::table('motif_demande')->insert([
            'id_motif_demande' => Constantes::ID_MOTIF_DEMANDE_HANDICAPE,
            'libelle_motif_demande' => "Demande d'accès à une place handicapée",
        ]);

        DB::table('motif_demande')->insert([
            'id_motif_demande' => Constantes::ID_MOTIF_ELEVATION_REFERENT,
            'libelle_motif_demande' => "Demande de changement de rôle : REFERENT",
        ]);

        DB::table('motif_demande')->insert([
            'id_motif_demande' => Constantes::ID_MOTIF_ELEVATION_ADMIN,
            'libelle_motif_demande' => "Demande de changement de rôle : ADMIN",
        ]);

        /* INSERTION TYPE DE VEHICULES */
        DB::table('type')->insert([
            'type_vehicule' => 'Voiture',
        ]);

        DB::table('type')->insert([
            'type_vehicule' => 'Moto',
        ]);

        /* INSERTION TYPOLOGIE VEHICULE */
        DB::table('typologie')->insert([
            'typologie_vehicule' => 'Véhicule personnel',
            'typologie_vehicule' => 'Véhicule de société',
        ]);
    }

}
