<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->string('code_promotion', 10)->primary();
            $table->date('date_debut');
            $table->date('date_fin');
            $table->integer('id_formation')->nullable()->index('promotion_formation0_fk');
            $table->integer('id_utilisateur_referent')->nullable()->index('promotion_utilisateur1_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion');
    }
}
