<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicule', function (Blueprint $table) {
            $table->string('immatriculation', 8)->primary();
            $table->string('couleur', 20);
            $table->integer('id_utilisateur')->index('vehicule_utilisateur0_fk');
            $table->integer('id_type_vehicule')->index('vehicule_type1_fk');
            $table->integer('id_modele')->index('vehicule_modele2_fk');
            $table->integer('id_typologie_vehicule')->index('vehicule_typologie3_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicule');
    }
}
