<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUtilisateurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilisateur', function (Blueprint $table) {
            $table->integer('id_utilisateur', true);
            $table->string('email', 60);
            $table->string('nom', 20);
            $table->string('prenom', 20);
            $table->string('password', 128)->nullable();
            $table->string('numero_telephone', 10)->nullable();
            $table->boolean('utilisateur_handicape')->nullable();
            $table->string('token')->nullable();
            $table->date('email_verified_at')->nullable();
            $table->integer('id_role')->nullable()->index('utilisateur_role0_fk');
            $table->string('code_promotion', 10)->nullable()->index('utilisateur_promotion1_fk');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilisateur');
    }
}
