<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUtilisateurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('utilisateur', function (Blueprint $table) {
            $table->foreign('code_promotion', 'utilisateur_promotion1_fk')->references('code_promotion')->on('promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_role', 'utilisateur_role0_fk')->references('id_role')->on('role')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utilisateur', function (Blueprint $table) {
            $table->dropForeign('utilisateur_promotion1_fk');
            $table->dropForeign('utilisateur_role0_fk');
        });
    }
}
