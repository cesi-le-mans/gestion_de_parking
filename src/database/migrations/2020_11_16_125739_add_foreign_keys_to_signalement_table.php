<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSignalementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('signalement', function (Blueprint $table) {
            $table->foreign('id_motif_signalement', 'signalement_motif_fk')->references('id_motif_signalement')->on('motif_signalement')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_utilisateur', 'signalement_idutilisateur_fk')->references('id_utilisateur')->on('utilisateur')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('immatriculation', 'signalement_immatriculation_fk')->references('immatriculation')->on('vehicule')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('signalement', function (Blueprint $table) {
            $table->dropForeign('signalement_motif_fk');
            $table->dropForeign('signalement_idutilisateur_fk');
            $table->dropForeign('signalement_immatriculation_fk');
        });
    }
}

