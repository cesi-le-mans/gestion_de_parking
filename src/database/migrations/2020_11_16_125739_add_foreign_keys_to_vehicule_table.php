<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToVehiculeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicule', function (Blueprint $table) {
            $table->foreign('id_modele', 'vehicule_modele2_fk')->references('id_modele')->on('modele')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_type_vehicule', 'vehicule_type1_fk')->references('id_type_vehicule')->on('type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_typologie_vehicule', 'vehicule_typologie3_fk')->references('id_typologie_vehicule')->on('typologie')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_utilisateur', 'vehicule_utilisateur0_fk')->references('id_utilisateur')->on('utilisateur')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicule', function (Blueprint $table) {
            $table->dropForeign('vehicule_marque2_fk');
            $table->dropForeign('vehicule_type1_fk');
            $table->dropForeign('vehicule_typologie3_fk');
            $table->dropForeign('vehicule_utilisateur0_fk');
        });
    }
}
