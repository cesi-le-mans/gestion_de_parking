<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotion', function (Blueprint $table) {
            $table->foreign('id_formation', 'promotion_formation0_fk')->references('id_formation')->on('formation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_utilisateur_referent', 'promotion_utilisateur1_fk')->references('id_utilisateur')->on('utilisateur')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotion', function (Blueprint $table) {
            $table->dropForeign('promotion_formation0_fk');
            $table->dropForeign('promotion_utilisateur1_fk');
        });
    }
}
