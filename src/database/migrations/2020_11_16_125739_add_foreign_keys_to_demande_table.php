<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDemandeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('demande', function (Blueprint $table) {
            $table->foreign('id_motif_demande', 'demande_motif_fk')->references('id_motif_demande')->on('motif_demande')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_utilisateur', 'demande_utilisateur_fk')->references('id_utilisateur')->on('utilisateur')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('demande', function (Blueprint $table) {
            $table->dropForeign('demande_motif_fk');
            $table->dropForeign('demande_utilisateur_fk');
        });
    }
}
