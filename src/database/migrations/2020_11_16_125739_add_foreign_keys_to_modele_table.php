<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToModeleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modele', function (Blueprint $table) {
            $table->foreign('id_marque', 'modele_marque0_fk')->references('id_marque')->on('marque')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modele', function (Blueprint $table) {
            $table->dropForeign('modele_marque0_fk');
        });
    }
}
