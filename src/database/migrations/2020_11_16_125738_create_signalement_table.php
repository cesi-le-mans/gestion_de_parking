<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignalementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signalement', function (Blueprint $table) {
            $table->integer('id_signalement', true);
            $table->integer('id_utilisateur')->index('signalement_idutilisateur_fk');
            $table->string('immatriculation', 8)->index('signalement_immatriculation_fk');
            $table->integer('id_motif_signalement')->index('signalement_motif_fk');;
            $table->date('date_signalement');
            $table->boolean('signalement_traite')->nullable();
            $table->string('signalement_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signalement');
    }
}
