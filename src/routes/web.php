<?php

use App\Http\Controllers\Back\AdminController;
use App\Http\Controllers\Back\UserAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes(['verify' => true]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('verified');;

Route::prefix('admin')->namespace('Back')->group(function () {
    Route::name('admin')->get('/', [AdminController::class, 'index']);

    //USER ROLE
    Route::name('users')->get('/users', [AdminController::class, 'users']);
    Route::name('utilisateurExport')->get('exportUser', [AdminController::class, 'utilisateurExport']);
    Route::name('utilisateurImport')->post('users', [AdminController::class, 'utilisateurImport']);
    //Route::name('addUser')->post('', [UserAdminController::class, 'create']);

    Route::name('roles')->get('/roles', [AdminController::class, 'roles']);
    //Route::name('addRoles')->post('', [UserAdminController::class, 'create']);

    //VEHICULE MODELE MARQUE TYPOLOGIE
    Route::name('vehicules')->get('/vehicules', [AdminController::class, 'vehicules']);
    //Route::name('addVehicule')->post('', [UserAdminController::class, 'create']);

    Route::name('modeles')->get('/modeles', [AdminController::class, 'modeles']);
    Route::name('modeleExport')->get('exportModele', [AdminController::class, 'modeleExport']);
    Route::name('modeleImport')->post('modeles', [AdminController::class, 'modeleImport']);
    //Route::name('addModele')->post('', [UserAdminController::class, 'create']);

    Route::name('marques')->get('/marques', [AdminController::class, 'marques']);
    Route::name('marqueExport')->get('exportMarque', [AdminController::class, 'marqueExport']);
    Route::name('marqueImport')->post('marques', [AdminController::class, 'marqueImport']);
    //Route::name('addMarque')->post('', [UserAdminController::class, 'create']);

    Route::name('typologies')->get('/typologies', [AdminController::class, 'typologies']);
    //Route::name('addTypologie')->post('', [UserAdminController::class, 'create']);

    Route::name('types')->get('/types', [AdminController::class, 'types']);
    //Route::name('addType')->post('', [UserAdminController::class, 'create']);

    //FORMATION PROMOTION
    Route::name('formations')->get('/formations', [AdminController::class, 'formations']);
    //Route::name('addFormation')->post('', [UserAdminController::class, 'create']);

    Route::name('promotions')->get('/promotions', [AdminController::class, 'promotions']);
    //Route::name('addPromotion')->post('', [UserAdminController::class, 'create']);

    //DEMANDE MOTIFDEMANDE MOTIFSIGNALEMENT
    Route::name('demandes')->get('/demandes', [AdminController::class, 'demandes']);
    //Route::name('addDemande')->post('', [UserAdminController::class, 'create']);


    Route::name('motifsDemandes')->get('/motifsDemandes', [AdminController::class, 'motifsDemandes']);
    //Route::name('addMotifDemande')->post('', [UserAdminController::class, 'create']);

    Route::name('motifsSignalements')->get('/motifsSignalements', [AdminController::class, 'motifsSignalements']);
    //Route::name('addMotifSignalement')->post('', [UserAdminController::class, 'create']);

    Route::name('signalements')->get('/signalements', [AdminController::class, 'signalements']);

    
});
