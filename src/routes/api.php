<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\DemandeController;
use App\Http\Controllers\FormationController;
use App\Http\Controllers\MarqueController;
use App\Http\Controllers\ModeleController;
use App\Http\Controllers\MotifDemandeController;
use App\Http\Controllers\MotifSignalementController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SignalementController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\TypologieController;
use App\Http\Controllers\UtilisateursController;
use App\Http\Controllers\VehiculeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('demande', DemandeController::class);

Route::apiResource('marque', MarqueController::class);

Route::apiResource('modele', ModeleController::class);

Route::apiResource('formation', FormationController::class);

Route::apiResource('motifDemande', MotifDemandeController::class);

Route::apiResource('motifSignalement', MotifSignalementController::class);

Route::apiResource('promotion', PromotionController::class);

Route::apiResource('role', RoleController::class);

Route::apiResource('signalement', SignalementController::class);

Route::apiResource('type', TypeController::class);

Route::apiResource('typologie', TypologieController::class);

Route::apiResource('utilisateur', UtilisateursController::class);

Route::apiResource('vehicule', VehiculeController::class);

Route::name('login')->post('/login', [LoginController::class, 'loginAPI']);

Route::name('register')->post('/register', [RegisterController::class, 'registerAPI']);


