# Projet Gestion de Parking

Pour installer le projet sous docker avec phpMyAdmin : `docker-compose up -d`.

Pour mettre à jour docker si neccessaire : `docker-compose build`

Puis aller dans le répertoire src et entrer la commande suivante : `docker-compose run --rm composer update`.

Pour initialiser la base de données il faut ensuite faire un `docker-compose run --rm artisan migrate:refresh --seed`

Ports utilisés : 

- **nginx** - `:8080`
- **mysql** - `:3306`
- **php** - `:9000`
- **phpmyadmin** - `:8081`


En cas d'erreur avec :
"Class '\App\Models\Utilisateur' not found" => `docker-compose run --rm composer dump-autoload`



